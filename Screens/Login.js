import React from 'react';
import {
  SafeAreaView, TouchableOpacity,ScrollView, View,Alert, Text,Image,Dimensions,AsyncStorage
} from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import {Header ,CheckBox,Input} from 'react-native-elements'

let styles = {
  scroll: {
      flex:1,
      top:-1,
    backgroundColor: '#444444',
  },
  container: {
    margin: 10,
    marginTop: Platform.select({ ios: 8, android: 32 }),
    flex: 1,
  },
  contentContainer: {
    padding: 8,
  },
  textContainer: {
    textAlign: 'center', // <-- the magic
    fontSize: 28,
    padding:50,
    color:'#F1803A'
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textaccountButton: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
  },
  safeContainer: {
    flex: 1,
    backgroundColor: '#444444',
  },
};

let defaults = {
  username: '',
  password: '',
};
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.onFocus = this.onFocus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitUserName = this.onSubmitUserName.bind(this);
    this.onSubmitPassword = this.onSubmitPassword.bind(this);
    this.usernameRef = this.updateRef.bind(this, 'username');
    this.passwordRef = this.updateRef.bind(this, 'password');
    this.renderPasswordAccessory = this.renderPasswordAccessory.bind(this);

    this.state = {
      hidePassword: true,
      secureTextEntry: true,
      isLoading: false,
      RoleId:'',
      EmailId: '',
      Password: '',
      phoneBR: '',
      ...defaults,
    };
  }
  onFocus() {
    let { errors = {} } = this.state;
    for (let name in errors) {
      let ref = this[name];
      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }
  validate = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      Alert.alert("Email is Not Correct.");
      this.setState({ email: text })
      return false;
    }
    else {
      this.setState({ email: text })
      console.log("Email is Correct");
      Alert.alert("Email is Correct.");
    }
  }

  onChangeText(text) {
    ['username', 'password']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
        }
      });
  }
  onSubmitUserName() {
    this.password.focus();

  }

  onSubmitPassword() {
    this.password.focus();
  }
  onSubmit() {
    if(this.state.EmailId == ''){
      Alert.alert("Please Enter The Email.");
    }
    else if(this.state.Password == ''){
      Alert.alert("Please Enter Password.");
    }
    else {
    fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "email": this.state.EmailId,
        "password": this.state.Password,
      })
    }).then((response) => response.json())
      .then((responseData) => {
       
        if (responseData.status === "Success") {
          console.log("responseData",responseData.data)
            var fname = responseData.data[0].first_name
            var lanme = responseData.data[0].last_name
            var fullName = fname.concat(" " , lanme);
            console.log("otp",responseData.data[0].otp)
            AsyncStorage.setItem('Photo', responseData.data[0].photo);
            AsyncStorage.setItem('LOGINSS', "SussesLogin");
            AsyncStorage.setItem('Id', responseData.data[0].id.toString());
            AsyncStorage.setItem('RoleId', responseData.data[0].role_id.toString());
            AsyncStorage.setItem('fullName', fullName);
            AsyncStorage.setItem('transactioncompleted', responseData.data[0].transaction_completed.toString());
              this.props.navigation.navigate('Dashboard', {})
            
        }
    
        else {
          Alert.alert(JSON.stringify(responseData.message));
          this.setState({ isLoading: false });
        }
      })
      .catch((error) => {
        console.error(error);
      })
    }
  }
  componentDidMount(){
    AsyncStorage.getItem('LOGINSS')
    .then((value) => {
      this.state.RoleId = value
      console.log("rpll", this.state.RoleId)
      if(!this.state.RoleId){
        //this.props.navigation.navigate('Dashboard', {})
      }
      else{
        this.props.navigation.navigate('Dashboard', {})
      }
    })
  }      

  updateRef(name, ref) {
    this[name] = ref;
  }
  _onEditTextField(type, value) {
    if (type == 'email') {
      this.setState({
        EmailId: value.toLowerCase()
      })
      console.log("Email Id :--> " + this.state.EmailId + " value :--> " + value);
    }
    else if (type == 'password') {
      this.setState({
        Password: value
      })
      console.log("Password :--> " + this.state.Password);
    }
    this._validateInput(type, value)
  }
  _validateInput(type, value) {

    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (type == 'email') {
      if (reg.test(this.state.EmailId) === false || this.state.EmailId.length == 0) {
        this.setState({ isEmailValidationFailed: true })
        return true;
      }
      else {
        this.setState({ isEmailValidationFailed: false });
        return false;
      }
    }
    else if (type == 'password') {
      if (this.state.Password.length == 0) {
        this.setState({ isPasswordValidationFailed: true })
        return true;
      } else {
        this.setState({ isPasswordValidationFailed: false })
        return false;
      }
    }
    else if (type == 'All') {

      if (this.state.EmailId.length == 0) {
        alert("Please Enter Email");
        this.setState({
          isEmailValidationFailed: true,
        })
        return true;
      }
      else if (reg.test(this.state.EmailId) === false) {
        alert("Please Enter Email.");
        this.setState({
          isEmailValidationFailed: true,
        })
        return true;
      }
      else if (this.state.Password.length == 0) {
        alert("Please Enter Password..");
        this.setState({
          isEmailValidationFailed: true,
        })
        return true;
      }
      else {
        this.setState({
          isEmailValidationFailed: false,
          isPasswordValidationFailed: false
        })
        return false;
      }
    }}

  renderPasswordAccessory() {
    let { secureTextEntry } = this.state;
    let name = secureTextEntry ?
      'visibility' :
      'visibility-off';
  }
 
  setPasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  }

  handleSignUpPress(){
    this.props.navigation.navigate('Signup', {})
  }
  render() {
    let { errors = {}, secureTextEntry, ...data } = this.state;
    let { username, password } = data;
    let defaultEmail = `${username || ''}${password || ''}`
      .replace(/\s+/g, '_')
      .toLowerCase();
    return (
      <SafeAreaView style={styles.safeContainer}>
          
        <ScrollView
          style={styles.scroll} 
          contentContainerStyle={styles.contentContainer}
          keyboardShouldPersistTaps='handled'
        >
       <View style={{  marginTop: 0, alignSelf:'center' }}>
 <Image  style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
          </View>
          <Text numberOfLines={1} style={styles.textContainer}>Sign In </Text>
          <View style={styles.container}>
         
          <Input
              ref={this.usernameRef}
              defaultValue={defaultEmail}
              keyboardType='email-address'
              autoCapitalize='none'
             placeholder='Email'
             onChangeText={value => this._onEditTextField('email', value)}

            //  onChangeText={email => this.setState({email})}
             inputStyle={{height:60,padding:10,color:'#fff'}}
             placeholderTextColor =  "#fff"
             leftIcon={
                <Image source={require('../Images/username.png')} />
                }
             />
           <Input
              ref={this.passwordRef}
              autoCapitalize='none'
             placeholder='Password'
            // onChangeText={pass => this.setState({pass})}
             onChangeText={value => this._onEditTextField('password', value)}
             inputStyle={{height:60,padding:10,color:'#fff'}}
             placeholderTextColor =  "#fff"
             secureTextEntry={this.state.hidePassword}
             leftIcon={
                <Image source={require('../Images/password.png')} />
                }
                rightIcon={
                  <TouchableOpacity activeOpacity={0.8} onPress={this.setPasswordVisibility}>
                <Image source={(this.state.hidePassword) ? require('../Images/seepass.png') : require('../Images/seepass.png')}  style={{
                 
                }} />
              </TouchableOpacity>
                }
             />
            
            <View>
          
             
            </View>
            <View style={styles.fixToText}>
            <TouchableOpacity onPress={this.handleforgotPress} >
            <View style={styles.fixToText}>
            <CheckBox 
  checkedIcon={<Image source={require('../Images/selected_remember.png')} />}
  uncheckedIcon={<Image source={require('../Images/unselected_remember.png')} />}
  checked={this.state.checked}
  onPress={() => this.setState({checked: !this.state.checked})}
  
/>
           
              <Text style={{  color:'#F1803A', fontSize: 18,top:12,marginStart:-20}} > Remember Me </Text>
              </View>
            </TouchableOpacity>
           
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')} style={{ height: 50, alignItems: 'center', justifyContent: 'center', }}>
              <Text style={{  color:'#F1803A', fontSize: 18 ,paddingRight:2}} > Forgot password? </Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity onPress={this.onSubmit}>
              <View style={{  padding: 15,
                 height: 50,margin:30, alignItems: 'center',
                  justifyContent: 'center',backgroundColor:'#F1803A',borderRadius: 25 ,}}>
              <Text style={{
                textAlign: 'center',color:'white',
                fontSize: 20,height:30
              }} >Sign In</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup', {})} >
            <View style={styles.textaccountButton}>
              <Text style={{ color: 'white', textAlign: 'justify', fontSize: 18 }}>
              New to AMR Group?
             <Text> </Text>
                <Text style={{ color: '#F1803A', textAlign: 'justify', fontSize: 18 }}>
                  SignUp
             </Text>
                <Text> </Text>
               
               
              </Text>

            </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default Login;