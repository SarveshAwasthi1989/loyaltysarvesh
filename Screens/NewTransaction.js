import React from 'react';
import { ScrollView, View, Text, Image, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import { ListItem, Header } from 'react-native-elements'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const list = [
    {
        name: 'H2H Hoodie',
        title: 'Mercedes C200 AMG 2020',
        price: 'Product: Saava Flask',
        //avatar_url: require(''),
        subtitle: 'Purchasedon: 12feb 2020 13:51',
        sold: '769 Sold out of 1260',
        tickect: 'Ticket Number',
        tickno: 'CR-00022-00464-D',
    },
    {
        name: 'H2H Hoodie',
        title: 'Mercedes C200 AMG 2020',
        price: 'Product: Saava Flask',
        // avatar_url: require(''),
        subtitle: 'Purchasedon: 12feb 2020 13:51',
        sold: '769 Sold out of 1260',
        tickect: 'Ticket Number',
        tickno: 'CR-00022-00464-D',
    },


]

class NewTransaction extends React.Component {
    constructor(props) {
        super(props);

    }

    keyExtractor = (item, index) => index.toString()
    renderItem = ({ item }) => (
        <View style={{ paddingLeft: 5, height: 145, width: width, marginTop: 5 }}>
            <View style={{ flexDirection: 'row', height: 145 }}>
                <View style={{ flexDirection: 'row', height: 140, width: width - 30, marginLeft: 10, backgroundColor: '#F1803A', borderRadius: 15, shadowOpacity: 6, shadowRadius: 10 }}>
                    <View style={{ flexDirection: 'column', marginLeft: 7, marginTop: 16, height: 110 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: '#fff',  fontWeight: 'bold', fontSize: 16, height: 20,shadowOpacity: 6, shadowRadius: 10 }}>
                                Name :-
             </Text>
                            <Text>  </Text>
                            <Text style={{ color: '#fff', fontSize: 15, height: 20 }}>
                                {item.title}
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16, height: 20,shadowOpacity: 6, shadowRadius: 10 }}>
                                Type :-
             </Text>
                            <Text>  </Text>
                            <Text style={{ color: '#fff', fontSize: 15, height: 20 }}>
                                {item.price}
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: '#fff',  fontWeight: 'bold', fontSize: 16, height: 20,shadowOpacity: 6, shadowRadius: 10 }}>
                                Status :-
             </Text>
                            <Text>  </Text>
                            <Text style={{color: '#fff',  fontSize: 15, height: 20 }}>
                                {item.price}
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: '#fff',  fontWeight: 'bold', fontSize: 16, height: 20,shadowOpacity: 6, shadowRadius: 10 }}>
                                Point :-
             </Text>
                            <Text>  </Text>
                            <Text style={{ color: '#fff',
                             fontSize: 15, height: 20 }}>
                                {item.price}
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16, height: 20,shadowOpacity: 6, shadowRadius: 10 }}>
                                Archive :-
             </Text>
                            <Text>  </Text>
                            <Text style={{color: '#fff',  fontSize: 15, height: 20 }}>
                                {item.price}
                            </Text>
                        </View>


                    </View>

                </View>
            </View>

        </View>
    )

    render() {

        return (
            <View style={{ backgroundColor: '#444444', flex: 1 }}>
                <ScrollView style={{ backgroundColor: '#444444', flex: 1 }}>
                    <View style={{ marginTop: 26, alignSelf: 'center' }}>
                        <Image style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
                    </View>
                    <FlatList style={{ marginTop: 5, marginBottom: 5 }}
                        keyExtractor={this.keyExtractor}
                        data={list}
                        renderItem={this.renderItem}
                    />
                </ScrollView>
            </View>
        );
    }
}

export default NewTransaction;