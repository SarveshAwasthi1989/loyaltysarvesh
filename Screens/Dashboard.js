import React from 'react';
import {
  Image, AsyncStorage, TouchableOpacity, ScrollView, View, Text, TouchableWithoutFeedback
} from 'react-native';
import { Drawer } from "react-native-material-drawer";
import { Header } from 'react-native-elements'
import FastImage from 'react-native-fast-image'
console.disableYellowBox = true;

const styles = {
  container: {
    width: "100%",
    height: "100%",
    top: -1
  },
  body: {
    //backgroundColor: "red",
    height: "100%",

  }
};
class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      Role_Id:'',
      RoleId:'',
      fullName:'',
      Photo:'',
      transactionArry: '',
      transactioncompleted:''
    };
  }
  componentDidMount() {
    this.state.isOpen = false
   
  }
  onSubmitLogOut = () => {
    AsyncStorage.removeItem("LOGINSS")
    AsyncStorage.removeItem("transactioncompleted")
    this.props.navigation.navigate('Login')
  }
  getmsgList(){
    AsyncStorage.getItem('transactioncompleted')
    .then((value) => {
      this.state.transactioncompleted = value
      console.log("transactioncompleted",this.state.transactioncompleted)
    });
    AsyncStorage.getItem('Id')
    .then((value) => {
      this.state.RoleId = value
    
      fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/ViewProfile', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "user_id": this.state.RoleId,
        })
      }).then((response) => response.json())
        .then((responseData) => {
          if (responseData.status === "Success") {
            this.state.transactionArry = responseData.data
            this.state.fastName = this.state.transactionArry[0].first_name
            this.state.lastname = this.state.transactionArry[0].last_name
            AsyncStorage.setItem('transactioncompleted', this.state.transactionArry[0].transaction_completed.toString());
            console.log("gyg",this.state.transactioncompleted)
            var fname = this.state.transactionArry[0].first_name
              var lanme = this.state.transactionArry[0].last_name
              this.state.fullName = fname.concat(" " , lanme);
            this.state.Photo = this.state.transactionArry[0].photo
           
          }
          else {
            //this.props.navigation.navigate('WelcomeScreen')
          }
          this.setState({
            isFetching: false,
          }, function () {
          });
        })
        
    });
  }
  componentWillMount(){
    //this.getmsgList();
    this.state.isOpen = false
    AsyncStorage.getItem('RoleId')
    .then((value) => {
      this.state.Role_Id = value
      
    })
    setInterval(() => {
      this.setState(() => {
        this.getmsgList();
        return { unseen: "does not display" }
      });
    }, 3000);
  }
  
  _onPressMoreButton() {
    AsyncStorage.getItem('RoleId')
    .then((value) => {
      this.state.Role_Id = value
      console.log("dRole_Id",this.state.Role_Id)
      if (this.state.Role_Id === "1") {
        //this.state.roalid = "Buyer"
        this.props.navigation.navigate('TransactionBuyer', {
        })
      }
      else if (this.state.Role_Id === "2") {
        //this.state.roalid = "Seller"
        this.props.navigation.navigate('TransactionSeller', {
        })
      }
      else if (this.state.Role_Id === "3") {
       // this.state.roalid = "Referrer"
        this.props.navigation.navigate('TransactionBuyer', {
        })
      }
      else if (this.state.Role_Id === "4") {
        //this.state.roalid = "Landlord"
        this.props.navigation.navigate('TransactionLandlord', {
        })
      }
      else if (this.state.Role_Id === "13") {
       // this.state.roalid = "Sales Associate"
        this.props.navigation.navigate('TransactionSalesAssociate', {
        })
      }
    
  })
  }
  CompleteTrans(){
    if(this.state.transactioncompleted == "0"){
      return<View style={{flex:1}}>
      <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 18, color: "#fff" }}>
      Dashboard Summary
          </Text>
  
          <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 16, color: "#fff" }}>
          COMPLETE YOUR PROFILE
          </Text>
  
          <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 15, color: "#fff" }}>
          Thanks for Verifying Your Account. Please Complete Your Profile
  
          </Text>
          <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 60 }}>
            <TouchableOpacity onPress={() => this._onPressMoreButton()} style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }} >
              <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 18 }}>
                Transaction
                      </Text>
            </TouchableOpacity>
            </View>
          </View>
    }
    else{

      return<View style={styles.container}>
      <View style={{ backgroundColor: '#444444', height: 250, top: -1, backgroundColor: '#5F5F5F' }}>
        <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 25, color: "#fff" }}>
        Dashboard
        </Text>
        <TouchableOpacity>
          <View style={{ alignItems: 'center' }}>
            <FastImage style={{ height: 100, width: 100, borderRadius: 50, marginTop: 20 }}
              source={{ uri: this.state.Photo }} />
          </View>
        </TouchableOpacity>
        <View>
          <Text style={{ textAlign: 'center', marginTop: 20, color: "white", fontSize: 20 }}>
          {this.state.fullName}
            </Text>
        </View>
      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', top: 20, padding: 20 }}>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile', {})} style={{ backgroundColor: '#444444', width: 150, height: 150 }}>
          <View style={{ backgroundColor: '#5F5F5F', height: 150, width: 150 }}>
            <Image style={{ height: 59, width: 40, marginTop: 32, marginLeft: 50 }}
              source={require('../Images/dashboardprofile.png')} />
            <Text style={{ textAlign: 'center', marginTop: 50, bottom: 24, color: "#F1803A", fontSize: 20 }}>
              Profile
                </Text>
          </View>
        </TouchableOpacity >
        <TouchableOpacity style={{ backgroundColor: '#444444', width: 150, height: 150, marginLeft: 10 }} onPress={() => this.props.navigation.navigate('ExistingTransaction')}>
          <View style={{ backgroundColor: '#5F5F5F', height: 150, width: 150 }}>
            <Image style={{ height: 47, width: 70, marginTop: 40, marginLeft: 40 }}
              source={require('../Images/transcations.png')} />
            <Text style={{ textAlign: 'center', marginTop: 55, bottom: 24, color: "#F1803A", fontSize: 20 }}>
              Transactions
                </Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'space-between', padding: 20 }}>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Assigned', {})} style={{ backgroundColor: '#444444', width: 150, height: 150 }}>
          <View style={{ backgroundColor: '#5F5F5F', height: 150, width: 150 }}>
            <Image style={{ height: 75, width: 50, marginTop: 30, marginLeft: 50 }}
              source={require('../Images/points.png')} />
            <Text style={{ textAlign: 'center', marginTop: 40, bottom: 24, color: "#F1803A", fontSize: 20 }}>
              My Points
                        </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('MarketPlace', {})} style={{ backgroundColor: '#444444', width: 150, height: 150, marginLeft: 10 }}>
          <View style={{ backgroundColor: '#5F5F5F', height: 150, width: 150 }}>
            <Image style={{ height: 60, width: 70, marginTop: 28, marginLeft: 40 }}
              source={require('../Images/marketplace.png')} />
            <Text style={{ textAlign: 'center', marginTop: 55, bottom: 24, color: "#F1803A", fontSize: 20 }}>
            Marketplace
                        </Text>

          </View>
        </TouchableOpacity>
      </View>


    </View>
    }
    
  }
  render() {

    return (

      <View style={{ backgroundColor: "#444444", flex: 1 }}>
       
        <Drawer
          open={this.state.isOpen}
          drawerContent={
            <ScrollView style={{ backgroundColor: "#444444", width: "101%",flex:1 }}>
            <View style={{ backgroundColor: "#444444", height: '100%', width: "101%",flex:1 ,marginBottom:20}}>
             
              <View style={{ backgroundColor: '#E9883A' }}>
                <TouchableOpacity>
                  <View style={{ alignItems: 'center' }}>
                    <FastImage style={{ height: 100, width: 100, borderRadius: 50, marginTop: 40 }}
                      source={{ uri: this.state.Photo }} />
                  </View>
                </TouchableOpacity>
                <View>
                  <Text style={{ textAlign: 'center', marginTop: 10, color: "#fff", fontSize: 24 ,height:50}}>
                  {this.state.fullName}
                    </Text>
                </View>
              </View>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile', {})}>
                <View style={{ flexDirection: 'row', height: 55, marginTop: 5, paddingLeft: 10 }} >
                  <Image style={{ marginTop: 15, paddingLeft: 10, }} source={require('../Images/sideprof.png')} />
                  <Text style={{ height: 50, paddingLeft: 15, marginTop: 10, fontSize: 18, color: '#fff' }}>Profile</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('ExistingTransaction')}>
                <View style={{ flexDirection: 'row', height: 50, marginTop: 5, paddingLeft: 10 }} >
                  <Image style={{ marginTop: 15, paddingLeft: 10 }} source={require('../Images/sidetrans.png')} />
                  <Text style={{ height: 50, paddingLeft: 10, marginTop: 10, fontSize: 18, color: '#fff' }}>All Transactions</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.props.navigation.navigate('Assigned')}>
                <View style={{ flexDirection: 'row', height: 50, marginTop: 5, paddingLeft: 10 }} >
                  <Image style={{ marginTop: 15, paddingLeft: 10 }} source={require('../Images/sidetext.png')} />
                  <Text style={{ height: 50, paddingLeft: 10, marginTop: 10, fontSize: 18, color: '#fff' }}>My Points</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('MarketPlace')}>
                <View style={{ flexDirection: 'row', height: 50, marginTop: 5, paddingLeft: 10 }} >
                  <Image style={{ marginTop: 15, paddingLeft: 10 }} source={require('../Images/sidedashboard.png')} />
                  <Text style={{ height: 50, paddingLeft: 10, marginTop: 10, fontSize: 18, color: '#fff' }}>Marketplace</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Redemhistory')}>
                <View style={{ flexDirection: 'row', height: 50, marginTop: 5, paddingLeft: 10 }} >
                  <Image style={{ marginTop: 15, paddingLeft: 10 }} source={require('../Images/sidedashboard.png')} />
                  <Text style={{ height: 50, paddingLeft: 10, marginTop: 10, fontSize: 18, color: '#fff' }}>Redeem History</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Tradehistory')}>
                <View style={{ flexDirection: 'row', height: 50, marginTop: 5, paddingLeft: 10 }} >
                  <Image style={{ marginTop: 15, paddingLeft: 10 }} source={require('../Images/sidedashboard.png')} />
                  <Text style={{ height: 50, paddingLeft: 10, marginTop: 10, fontSize: 18, color: '#fff' }}>Trade History</Text>
                </View>
              </TouchableOpacity>
              <Text style={{ color: '#E9883A', paddingLeft: 8 }}>- - - - - - - - - - - - - - - - - - - - - - - </Text>

              <View style={{ flexDirection: 'row', height: 50, marginTop: 5, paddingLeft: 10 }} >
                <Image style={{ marginTop: 15, paddingLeft: 10 }} source={require('../Images/sidetext.png')} />
                <Text style={{ height: 50, paddingLeft: 10, marginTop: 10, fontSize: 18, color: '#fff' }}>F.A.Q.</Text>
              </View>

              <View style={{ flexDirection: 'row', height: 50, marginTop: 5, paddingLeft: 10 }} >
                <Image style={{ marginTop: 15, paddingLeft: 10 }} source={require('../Images/sideiabout_pp.png')} />
                <Text style={{ height: 50, paddingLeft: 10, marginTop: 10, fontSize: 18, color: '#fff' }}>Help & Support</Text>
              </View>
              <View style={{ flexDirection: 'row', height: 50, marginTop: 5, paddingLeft: 10 }} >
                <Image style={{ marginTop: 15, paddingLeft: 10 }} source={require('../Images/sidehelp.png')} />
                <Text style={{ height: 50, paddingLeft: 10, marginTop: 10, fontSize: 18, color: '#fff' }}>Contact Us</Text>
              </View>
              <View style={{ flexDirection: 'row', height: 50, marginTop: 5, paddingLeft: 10 }} >
                <Image style={{ marginTop: 15, paddingLeft: 10 }} source={require('../Images/sideiabout_pp.png')} />
                <Text style={{ height: 50, paddingLeft: 10, marginTop: 10, fontSize: 18, color: '#fff' }}>About App</Text>
              </View>
              {/* <Text style={{ color: '#E9883A', paddingLeft: 8 }}>- - - - - - - - - - - - - - - - - - - - - - - </Text> */}
              <TouchableOpacity onPress={this.onSubmitLogOut}>
              <View style={{ flexDirection: 'row', height: 50, marginTop: 5, paddingLeft: 10 }} >
                <Image style={{ marginTop: 15, paddingLeft: 10 }} source={require('../Images/sidelogout.png')} />
                <Text style={{ height: 50, paddingLeft: 10, marginTop: 10, fontSize: 18, color: '#fff' }}>Logout</Text>
              </View>
              </TouchableOpacity>

              {/* AsyncStorage.setItem('LOGINSS', "SussesLogin"); */}

            </View>
            </ScrollView>
          }
          onClose={() => this.setState({ isOpen: false })}
          animationTime={250}
        >
          <ScrollView contentContainerStyle={{ flexGrow: 1 }} style={{ backgroundColor: '#444444'}} >

<View style={{backgroundColor: "#444444"}}>
<View style={{ flexDirection: 'row', justifyContent: 'space-between',borderBottomWidth:1,borderBottomColor:'#F1803A' }}>
            <TouchableWithoutFeedback onPress={() => this.setState({ isOpen: !this.state.isOpen })}>
              <View style={{ top: 50, height: 50, width: 50, paddingLeft: 20 }}>
                <Image style={{ top: 0 }} source={require('../Images/dashboardside.png')} resizeMode="cover" />
              </View>
            </TouchableWithoutFeedback>
            <View style={{ marginTop: 0, alignSelf: 'center' }}>
              <Image style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
            </View>
            <TouchableWithoutFeedback onPress={() => {
              this.setModalVisible(true);
            }}>
              <View style={{ marginTop: 50,  height: 50, width: 50, }}>

              </View>
            </TouchableWithoutFeedback>
          </View>
          {this.CompleteTrans()}
         

          
          </View>
          </ScrollView>
        </Drawer>
       
      
      </View>


    );
  }
}

export default Dashboard;