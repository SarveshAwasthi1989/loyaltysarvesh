import React from 'react';
import {
  SafeAreaView, TouchableOpacity,ScrollView, View, Text, Image,Dimensions,TouchableWithoutFeedback,Alert
} from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import {Header,Input } from 'react-native-elements'
let styles = {
  scroll: {
      flex:1,
      top:-1,
    backgroundColor: '#444444',
  
  },
 
  container: {
    margin: 20,
    marginTop: Platform.select({ ios: 8, android: 32 }),
    flex: 1,
  },
  contentContainer: {
    padding: 8,
  },
  arrowbuttonContainer: {
    paddingTop: 18,
    margin: 8,
    flexDirection: 'row-reverse'
  },
  buttonContainer: {
    flex: 1,
    backgroundColor: "white",
    alignItems: 'center',
    justifyContent: 'center',
  },
  textContainer: {
    textAlign: 'center', // <-- the magic
    fontSize: 28,
    marginTop: 30,
    padding:50,
    color:'#F1803A'
  },
  textContainerone: {
    textAlign: 'left', // <-- the magic
    fontSize: 20,
    marginLeft: 20,
    width: 200,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 20,
  },
  instaButton: {
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1D7AE5',
    borderRadius: 30,
    borderColor: "rgba(255,255,255,0.7)",
    margin: 20,
  },
  createaccountButton: {
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    borderRadius: 30,
    borderColor: "grey",
    //borderTopColor: '#1D7AE5',
    borderWidth: 1,
  },
  textaccountButton: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
  },
  safeContainer: {
    flex: 1,
    backgroundColor: '#444444',
  },
  touachableButton: {
    position: 'absolute',
    right: 3,
    height: 40,
    width: 35,
    padding: 2
  },
};

let defaults = {
  username: '',
  password: '',
};
class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.onFocus = this.onFocus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitUserName = this.onSubmitUserName.bind(this);
    this.onSubmitPassword = this.onSubmitPassword.bind(this);
    this.usernameRef = this.updateRef.bind(this, 'username');
    this.passwordRef = this.updateRef.bind(this, 'password');
    this.renderPasswordAccessory = this.renderPasswordAccessory.bind(this);

    this.state = {
      hidePassword: true,
      secureTextEntry: true,
      isLoading: false,
      Email:'',
      ...defaults,
    };
  }
  onFocus() {
    let { errors = {} } = this.state;
    for (let name in errors) {
      let ref = this[name];
      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }


  onChangeText(text) {
    ['username', 'password']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
        }
      });
  }
  onSubmitUserName() {
    this.password.focus();

  }

  onSubmitPassword() {
    this.password.focus();
  }
  onSubmit() {
    let errors = {};

    ['username', 'password']
      .forEach((name) => {
        let value = this[name].value();

        if (!value) {
          errors[name] = 'Should not be empty';
        } else {
          if ('password' === name && value.length < 6) {
            errors[name] = 'Too short';
          }
          else {
           
          }
        }
      });

    this.setState({ errors });
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  renderPasswordAccessory() {
    let { secureTextEntry } = this.state;
    let name = secureTextEntry ?
      'visibility' :
      'visibility-off';
  }
 
  setPasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  }

  handleSignUpPress(){
    this.props.navigation.navigate('Signup', {})
  }
  onSubmit() {
    if(this.state.Email == '' ){
      Alert.alert("Please Enter the Email");
    }
    else{
    fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/ForgotPassword', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "email": this.state.Email,
      })
    }).then((response) => response.json())
      .then((responseData) => {
        this.setState({ isLoading: false });
        if (responseData.status === "Success") {
          Alert.alert(JSON.stringify(responseData.message));
        }
        else {
          this.setState({ isLoading: false });
         console.log("responseData.message",responseData.message)
         // Alert.alert(JSON.stringify(responseData[0].message));
          //this.props.navigation.navigate('Dashboard', {})
    
        }
      })
      .catch((error) => {
        console.error(error);
      })
    }
  }
          
  render() {
    let { errors = {}, secureTextEntry, ...data } = this.state;
    let { username, password } = data;
    let defaultEmail = `${username || ''}${password || ''}`
      .replace(/\s+/g, '_')
      .toLowerCase();
    return (
      <SafeAreaView style={styles.safeContainer}>
          
       
        <ScrollView
          style={styles.scroll} 
          contentContainerStyle={styles.contentContainer}
          keyboardShouldPersistTaps='handled'
        >
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Login')}>
          <View style={styles.contentContainer}>
            <Image source={require('../Images/profileback.png')} resizeMode="cover" />
          </View>
        </TouchableWithoutFeedback>
      <View style={{  marginTop: 0, alignSelf:'center' }}>
 <Image  style={{height:80,width:200}} source={require('../Images/logo2.jpeg')}resizeMode="cover" />
          </View>
          <Text numberOfLines={1} style={styles.textContainer}>Forgot Password </Text>
          <View style={styles.container}>
          <Input
              keyboardType="email-address"
              onChangeText={Email => this.setState({Email})}
              autoCapitalize='none'
              placeholder='E-mail'
              inputStyle={{height:60,padding:10,color:'#fff'}}
              placeholderTextColor =  "#fff"
              leftIcon={
                <Image source={require('../Images/username.png')} />
                }
             />
           
            <View>
             
            </View>
            

            <TouchableOpacity  onPress={this.onSubmit} >
              
              <View style={{ fontSize: 20, fontSize: 16, padding: 15,
                 height: 50,margin:30, alignItems: 'center',
                  justifyContent: 'center',backgroundColor:'#F1803A',borderRadius: 25 ,}}>
              <Text style={{
                textAlign: 'center',color:'white',
                fontSize: 20, fontSize: 18, padding: 15,
                 height: 50,margin:30, alignItems: 'center',
                  justifyContent: 'center',backgroundColor:'#F1803A',borderRadius: 25 ,
               
              }} >Forgot Password</Text>
              </View>
            </TouchableOpacity>
           
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default ForgotPassword;