import { TextInput } from 'react-native-gesture-handler';
import React from 'react';
import {
    Image, Dimensions, TouchableOpacity, ScrollView, View, Text, TouchableWithoutFeedback, KeyboardAvoidingView
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { Header, Input } from 'react-native-elements'
import { Dropdown } from 'react-native-material-dropdown';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
let property_types = [{
    value: 'Single-Family Homes',
}, {
    value: 'Condominium',
}, {
    value: 'Townhouse',
},
{
    value: 'Multi-Family Home',
}
];
class TransactionSalesAssociate extends React.Component {

    constructor(props) {
        super(props);

    }

    setTimeValue = (value) => {
        this.state.timeValue = value
        console.log("this.state.timeValue ", this.state.timeValue)
    }
    saveDetails() {
        if (this.state.bedroomsText == '' || this.state.bathroomText == ''
            || this.state.propertytype == '' || this.state.parkingno == ''
            || this.state.YearBuilt == '' || this.state.minprice == '' || this.state.maxprice == ''
            || this.state.additionalcomments == '' || this.state.basementType == ''
            || this.state.basementStatus == '' || this.state.loanApproved == '') {
            Alert.alert("Please Enter All the Values.");
        }
        else {
            AsyncStorage.getItem('RoleId')
                .then((value) => {
                    this.state.RoleId = value
                    console.log("RoleId", this.state.RoleId)

                    AsyncStorage.getItem('Id')
                        .then((value) => {
                            this.state.userId = value
                            console.log("userId", this.state.userId)
                            fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/CreateTransaction', {
                                method: 'POST',
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify({
                                    "user_id": this.state.userId,
                                    "role_id": this.state.RoleId,
                                    "no_bedrooms": this.state.bedroomsText,
                                    "no_bathrooms": this.state.bathroomText,
                                    "property_type": this.state.propertytype,
                                    "no_parking_spaces": this.state.parkingno,
                                    "year_built": this.state.YearBuilt,
                                    "listing_price_min": this.state.minprice,
                                    "listing_price_max": this.state.maxprice,
                                    "additional_comments": this.state.additionalcomments,
                                    "besement_type": this.state.basementType,
                                    "basement_status": this.state.basementStatus,
                                    "loan_aapproved": this.state.loanApproved,
                                })
                            }).then((response) => response.json())
                                .then((responseData) => {
                                    this.setState({ isLoading: false });
                                    console.log("ffy", responseData)
                                    if (responseData.status === "Success") {
                                        Alert.alert(JSON.stringify(responseData.message));
                                        this.props.navigation.navigate('Dashboard', {})
                                    }
                                    else {

                                    }

                                })
                                .catch((error) => {
                                    console.error(error);
                                })
                        })
                })
        }
    }
    render() {

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between',borderBottomWidth:1,borderBottomColor:'#F1803A' ,backgroundColor:'#444444'}}>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ExistingTransaction')}>
                <View style={{ top: 50, height: 50, width: 50, paddingLeft: 20 }}>
            
                            <Image source={require('../Images/profileback.png')} resizeMode="cover" />
                        </View>
                    </TouchableWithoutFeedback>
            <View style={{ marginTop: 20, alignSelf: 'center' }}>
              <Image style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
            </View>
            
              <View style={{ top: 50,  height: 50, width: 50, }}>

              </View>
            
          </View>
                <ScrollView
                    style={{ flex: 1, backgroundColor: '#444444' }}
                    keyboardShouldPersistTaps='handled'
                >
                    <View style={{ alignItems: 'center' }}>
                        <Image style={{ height: 100, width: 100, borderRadius: 50, marginTop: 20 }}
                            source={require('../Images/profilee.png')} />
                    </View>
                    <TouchableOpacity>
                        <View style={{ alignItems: 'center' }}>
                            <Image style={{ height: 30, width: 30, borderRadius: 50, marginTop: -25, marginLeft: 80 }}
                                source={require('../Images/profilecam.png')} />
                        </View>
                    </TouchableOpacity>
                    <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 20, color: '#fff' }}>
                        Shreee Aedula
                                </Text>
                    {/* <Text style={{ marginBottom: 10, color: '#F1803A', paddingLeft: 10, fontSize: 17, marginTop: 20, }}>
                        Property types
                                </Text> */}
                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select Property Types'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 18 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={property_types}
                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={firstName => this.setState({ firstName })}
                        inputStyle={{ height: 60, color: '#fff' }}
                        placeholderTextColor="#fff"
                        placeholder='Enter a location'

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={firstName => this.setState({ firstName })}
                        inputStyle={{ height: 60, color: '#fff' }}
                        placeholderTextColor="#fff"
                        placeholder='City'

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={firstName => this.setState({ firstName })}
                        inputStyle={{ height: 60, color: '#fff' }}
                        placeholderTextColor="#fff"
                        placeholder='State'

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={firstName => this.setState({ firstName })}
                        inputStyle={{ height: 60, color: '#fff' }}
                        placeholderTextColor="#fff"
                        placeholder='Zipcode'
                        keyboardType={'numeric'}
                        maxLength={5}

                    />

                    <TextInput style={{ height: 100, margin: 20, borderColor: 'gray', borderWidth: 1, borderRadius: 20 }}>
                    </TextInput>
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10 }}>
                        <TouchableOpacity style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }}>
                            <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10 }}>
                                Create Transaction
                            </Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </View>


        );
    }
}

export default TransactionSalesAssociate;

