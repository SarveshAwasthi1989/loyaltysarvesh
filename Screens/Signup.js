import React from 'react';
import {
  SafeAreaView, TouchableOpacity, ScrollView, View, AsyncStorage, Text, Image, Alert, Dimensions, Modal,
  TouchableHighlight, TextInput, KeyboardAvoidingView,Platform
} from 'react-native';
import { Input } from 'react-native-elements'
import { Dropdown } from 'react-native-material-dropdown';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { TextInputMask } from 'react-native-masked-text'
import OtpInputs from "react-native-otp-inputs";
const keyboardVerticalOffset = Platform.OS === 'ios' ? 10 : 0

let max_price_range = [{
  value: 'Referrer',
}, {
  value: 'Buyer',
}, {
  value: 'Seller',
}, {
  value: 'Landlord',
}, {
  value: 'Sales Associate',
},
];

let styles = {
  scroll: {
    flex: 1,
    backgroundColor: '#444444',
  },
  container: {
    flex: 1,
    top: 40
  },
  textContainer: {
    textAlign: 'center', // <-- the magic
    fontSize: 28,
    top: 25,
    color: '#F1803A'
  },
  textaccountButton: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    height: 50
  },
  safeContainer: {
    flex: 1,
    backgroundColor: '#444444',
  },

};

let defaults = {
  username: '',
  password: '',
};
class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.onFocus = this.onFocus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitPassword = this.onSubmitPassword.bind(this);
    this.usernameRef = this.updateRef.bind(this, 'username');
    this.passwordRef = this.updateRef.bind(this, 'password');

    this.state = {
      hidePassword: true,
      secureTextEntry: true,
      timeValue: '',
      firstName: '',
      lastName: '',
      Email: '',
      pass: '',
      confirmpass: '',
      Phonenumber: '',
      usertype: '',
      modalVisible: false,
      otpvalue: '',
      userid: '',
      roalid: '',
      otpfValue: '',
      otpSValue: '',
      otptValue: '',
      otpfoValue: '',
      ...defaults,
    };
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  setPasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  }
  _onPressMoreButton() {
   
    if (this.state.otpfValue == '' || this.state.otpSValue == '' || this.state.otptValue == '' || this.state.otpfoValue == '') {
      Alert.alert("Please Fill The OTP");
    }
    else {
      this.state.otpvalue = this.state.otpfValue + this.state.otpSValue + this.state.otptValue + this.state.otpfoValue

      AsyncStorage.getItem('Id')
        .then((value) => {
          this.state.userid = value
          fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/verifyOTP', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "user_id": this.state.userid,
              "otp": this.state.otpvalue,
            })
          }).then((response) => response.json())
            .then((responseData) => {
              console.log("responseData.message", responseData)
              if (responseData.status === "Success") {
                var fname = responseData.data[0].first_name
                var lanme = responseData.data[0].last_name
                var fullName = fname.concat(" ", lanme);
                AsyncStorage.setItem('LOGINSS', "SussesLogin");
                AsyncStorage.setItem('Photo', responseData.data[0].photo);
                AsyncStorage.setItem('Id', responseData.data[0].id.toString());
                AsyncStorage.setItem('transactioncompleted', responseData.data[0].transaction_completed.toString());
                AsyncStorage.setItem('RoleId', responseData.data[0].role_id.toString());
                AsyncStorage.setItem('fullName', fullName);
                this.setModalVisible(!this.state.modalVisible);
                this.props.navigation.navigate('Dashboard', {})
              }
              else {
                Alert.alert(JSON.stringify(responseData.message));
              }

            })
            .catch((error) => {
              console.error(error);
            })
        })
    }

  }
  onFocus() {
    let { errors = {} } = this.state;
    for (let name in errors) {
      let ref = this[name];
      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }


  onChangeText(text) {
    ['username', 'password']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
        }
      });
  }

  setTimeValue = (value) => {
    this.state.timeValue = value
  }
  onSubmitPassword() {
    this.password.focus();
  }


  updateRef(name, ref) {
    this[name] = ref;
  }
  componentWillMount() {
    // fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/settings', {
    //   method: 'GET',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json'
    //   },

    // }).then((response) => response.json())
    //   .then((responseData) => {

    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   })
  }
  validate = (Email) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(Email) === false) {
      this.setState({ Email })
      return false;
    }
    else {
      this.setState({ Email })
   
    }
  }
  onSubmit() {

    if (this.state.timeValue === "Buyer") {
      this.state.roalid = 1
    }
    else if (this.state.timeValue === "Seller") {
      this.state.roalid = 2
    }
    else if (this.state.timeValue === "Referrer") {
      this.state.roalid = 3
    }
    else if (this.state.timeValue === "Landlord") {
      this.state.roalid = 4
    }
    else if (this.state.timeValue === "Sales Associate") {
      this.state.roalid = 13
    }

    if (this.state.firstName == '') {
      Alert.alert("Please Enter First Name");
      return;
    }
    else if (this.state.lastName == '') {
      Alert.alert("Please Enter Last Name");
      return;
    }
    else if (this.state.Email == '') {
      Alert.alert("Please Enter Email");
      return;

    }
    else if (this.state.pass == '') {
      Alert.alert("Please Enter Password");
      return;

    }
    else if (this.state.confirmpass == '') {
      Alert.alert("Please Enter Confirm Password");
      return;

    }
    else if (this.state.Phonenumber == '') {
      Alert.alert("Please Enter Phone number");
      return;

    }
    else if (this.state.roalid == '') {
      Alert.alert("Please fill the user type");
      return;
    }
    if (this.state.pass !== this.state.confirmpass) {
      alert("Passwords don't match");
      return;
    }
    if (this.state.pass.length < 8) {
      Alert.alert("Your text is less than what is required.");
      return;
    }
    if (this.state.confirmpass.length < 8) {
      Alert.alert("Your text is less than what is required.");
      return;
    }

    else {
      fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/register', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "first_name": this.state.firstName,
          "email": this.state.Email,
          "password": this.state.pass,
          "confirm_password": this.state.confirmpass,
          "last_name": this.state.lastName,
          "phone": this.state.Phonenumber,
          "role_id": this.state.roalid
        })
      }).then((response) => response.json())
        .then((responseData) => {
          if (responseData.status === "Success") {
            console.log("OTP",responseData.data[0].otp.toString())
            AsyncStorage.setItem('Id', responseData.data[0].id.toString());
            AsyncStorage.setItem('Otp', responseData.data[0].otp.toString());
            this.setModalVisible(!this.state.modalVisible);
          }
          else {
            Alert.alert(JSON.stringify(responseData.message));
          }
        })
        .catch((error) => {
          console.error(error);
        })
    }
  }


  render() {

    return (
      <SafeAreaView style={styles.safeContainer}>

        <ScrollView
          style={styles.scroll}
        >
          <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={keyboardVerticalOffset}>

            <View style={{ justifyContent: 'center', alignItems: 'center', top: 20 }}>
              <Image style={{ height: 100, width: 100, borderRadius: 50 }} source={require('../Images/dashboardpro.png')} />
            </View>
            <Text numberOfLines={1} style={styles.textContainer}>Sign Up </Text>

            <View style={styles.container}>
              <Input
                autoCapitalize='none'
                onChangeText={firstName => this.setState({ firstName })}
                inputStyle={{ height: 60, padding: 10, color: '#fff' }}
                placeholderTextColor="#fff"
                placeholder='First Name'
                autoCapitalize='sentences'
                leftIcon={
                  <Image source={require('../Images/namee.png')} />
                }
              />
              <Input
                onChangeText={lastName => this.setState({ lastName })}
                autoCapitalize='none'
                placeholder='Last Name'
                autoCapitalize='sentences'
                inputStyle={{
                  height: 60, padding: 10
                  , color: '#fff'
                }}
                placeholderTextColor="#fff"
                leftIcon={
                  <Image source={require('../Images/namee.png')} />
                }
              />
              <Input
                keyboardType="email-address"
                // onChangeText={Email => this.setState({Email})}
                onChangeText={(Email) => this.validate(Email)}
                autoCapitalize='none'
                placeholder='E-mail'
                inputStyle={{ height: 60, padding: 10, color: '#fff' }}
                placeholderTextColor="#fff"
                leftIcon={
                  <Image source={require('../Images/username.png')} />
                }
              />
              <View>
              <Input
                autoCapitalize='none'
                placeholder='Password'
                inputStyle={{height: 60, padding: 10, color: '#fff' }}
                placeholderTextColor="#fff"
                onChangeText={pass => this.setState({ pass })}
                value={this.state.pass}
                secureTextEntry={this.state.hidePassword}
                leftIcon={
                  <Image source={require('../Images/password.png')} />
                }
                rightIcon={
                  <TouchableOpacity activeOpacity={0.8} onPress={this.setPasswordVisibility}>
                <Image source={(this.state.hidePassword) ? require('../Images/seepass.png') : require('../Images/seepass.png')}  style={{
                 
                }} />
              </TouchableOpacity>
                }
                // rightIcon={
                //   <Image source={require('../Images/profileedit.png')} />
                // }
              />
                {/* <Input
                  onChangeText={pass => this.setState({ pass })}
                  autoCapitalize='none'
                  placeholder='Password'
                  secureTextEntry={true}
                  maxLength={15}
                  inputStyle={{ height: 60, padding: 10, color: '#fff' }}
                  placeholderTextColor="#fff"
                  leftIcon={
                    <Image source={require('../Images/password.png')} />
                  }
                /> */}
                <Input
                autoCapitalize='none'
                placeholder='Confirm Password'
                inputStyle={{height: 60, padding: 10, color: '#fff' }}
                placeholderTextColor="#fff"
                onChangeText={confirmpass => this.setState({ confirmpass })}
                value={this.state.confirmpass}
                secureTextEntry={this.state.hidePassword}
                leftIcon={
                  <Image source={require('../Images/password.png')} />
                }
                rightIcon={
                  <TouchableOpacity activeOpacity={0.8} onPress={this.setPasswordVisibility}>
                <Image source={(this.state.hidePassword) ? require('../Images/seepass.png') : require('../Images/seepass.png')}  style={{
                 
                }} />
              </TouchableOpacity>
                }
                // rightIcon={
                //   <Image source={require('../Images/profileedit.png')} />
                // }
              />
                {/* <Input
                  onChangeText={confirmpass => this.setState({ confirmpass })}
                  autoCapitalize='none'
                  maxLength={15}
                  placeholder='Confirm Password'
                  secureTextEntry={true}
                  inputStyle={{ height: 60, padding: 10, color: '#fff' }}
                  placeholderTextColor="#fff"
                  leftIcon={
                    <Image source={require('../Images/password.png')} />
                  }
                /> */}

                <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: "grey", margin: 10, paddingRight: 90, height: 50 }}>
                  <Image style={{ marginTop: 15, marginLeft: 15 }} source={require('../Images/phone.png')} />

                  <TextInputMask
                    type={'custom'}
                    options={{
                      // maskType: 'INTERNATIONAL',
                      // withDDD: false,
                      // dddMask: '(999) '
                      mask: '999-999-9999'
                    }}

                    autoCapitalize='none'
                    placeholder='Phone Number'
                    placeholderTextColor="#fff"
                    keyboardType={'phone-pad'}
                    value={this.state.Phonenumber}
                    onChangeText={Phonenumber => {
                      this.setState({
                        Phonenumber
                      })
                    }}

                    style={{
                      height: 50,
                      width: '80%',
                      color: '#fff',
                      paddingLeft: 10,
                      fontSize: 18
                    }}
                  />
                </View>
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center',marginTop:7 }}>
                <Text style={{ color: '#F1803A', fontSize: 18 }} > User Type </Text>
              </View>

              <View>
                <Dropdown

                  containerStyle={{ width: width - 20, paddingLeft: 10 }}
                  label='Select User Type'
                  textColor={'#fff'}
                  labelFontSize={18}
                  selectedItemColor={'#fff'}
                  itemColor={'#fff'}
                  style={{ color: 'white', paddingLeft: 1, fontSize: 18 }} //for changed text color
                  baseColor="rgba(255, 255, 255, 1)" //for initial text color
                  data={max_price_range}
                  onChangeText={timeValue => this.setState({ timeValue })}
                  value={this.state.timeValue}


                />
              </View>

              <TouchableOpacity onPress={this.onSubmit} >
                <View style={{
                  fontSize: 20, fontSize: 16, padding: 15,
                  height: 50, margin: 30, alignItems: 'center',
                  justifyContent: 'center', backgroundColor: '#F1803A', borderRadius: 10,
                }}>
                  <Text style={{
                    textAlign: 'center', color: 'white',
fontSize: 18
                  }} >Sign Up</Text>
                </View>
              </TouchableOpacity>

            </View>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login', {})} >
              <View style={styles.textaccountButton}>
                <Text style={{ color: 'white', textAlign: 'justify', fontSize: 18 }}>
                  Alerady have an account?
             <Text> </Text>
                  <Text style={{ color: '#F1803A', textAlign: 'justify', fontSize: 18 }}>
                    Sign In
             </Text>

                </Text>

              </View>
            </TouchableOpacity>

            <Modal style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 10,
              backgroundColor: 'blue'
            }}
              //animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                alert('Modal has been closed.');
              }}>
              <View style={{ marginTop:Platform.OS === 'ios' ? 260 : 0 , backgroundColor: '#444444', height: 200, borderColor: '#fff', borderRadius: 20, borderWidth: 1, margin: 20 }}>
                <TouchableHighlight style={{ height: 60, width: 60, position: 'absolute', right: 8 }}
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <View style={{ height: 30, width: 30, position: 'absolute', right: 8, top: 8, color: '#000', backgroundColor: '#fff', borderRadius: 15 }}>
                    <Text style={{ height: 30, width: 30, color: '#000', borderRadius: 15, top: 6, paddingLeft: 10 }}>X</Text>

                  </View>
                </TouchableHighlight>
                <OtpInputs
                  clearTextOnFocus
                  handleChange={code => console.log(code)}

                  keyboardType="phone-pad"
                  numberOfInputs={4}
                  //ref={otpRef}
                  selectTextOnFocus={false}
                />
                <View style={{
                  flexDirection: 'row', justifyContent: 'space-around', margin: 10,
                  marginTop: 60
                }}>


                  <TextInput style={{ height: 40, width: 40, backgroundColor: 'grey', color: 'white', fontSize: 18, paddingLeft: 10 }}
                    maxLength={1}
                    onChangeText={otpfValue => this.setState({ otpfValue })}
                    value={this.state.otpfValue}
                  />
                  <TextInput style={{ height: 40, width: 40, backgroundColor: 'grey', color: 'white', fontSize: 18, paddingLeft: 10 }}
                    maxLength={1}
                    onChangeText={otpSValue => this.setState({ otpSValue })}
                    value={this.state.otpSValue}
                  />

                  <TextInput style={{ height: 40, width: 40, backgroundColor: 'grey', color: 'white', fontSize: 18, paddingLeft: 10 }}
                    maxLength={1}
                    onChangeText={otptValue => this.setState({ otptValue })}
                    value={this.state.otptValue}
                  />

                  <TextInput style={{ height: 40, width: 40, backgroundColor: 'grey', color: 'white', fontSize: 18, paddingLeft: 10 }}
                    maxLength={1}
                    onChangeText={otpfoValue => this.setState({ otpfoValue })}
                    value={this.state.otpfoValue}
                  />


                </View>
                <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                  <TouchableOpacity onPress={() => this._onPressMoreButton()} style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }} >
                    <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 18 }}>
                      Submit OTP
                            </Text>
                  </TouchableOpacity>

                </View>
              </View>

            </Modal>
          </KeyboardAvoidingView>
        </ScrollView>

      </SafeAreaView>
    );
  }
}

export default Signup;