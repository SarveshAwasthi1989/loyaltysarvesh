import { TextInput } from 'react-native-gesture-handler';
import React from 'react';
import {
    Image, Dimensions, TouchableOpacity, ScrollView, View, Text, TouchableWithoutFeedback, KeyboardAvoidingView, Alert, AsyncStorage
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import RNGooglePlaces from 'react-native-google-places';
const keyboardVerticalOffset = Platform.OS === 'ios' ? 5 : 0
import { Header, Input } from 'react-native-elements'
import { Dropdown } from 'react-native-material-dropdown';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
// let max_price_range = [{
//     value: '$ 15000',
// }, {
//     value: '$ 20000',
// },
// {
//     value: '$ 25000',
// },
// {
//     value: '$ 30000',
// }
// ];

let YearBuilt = [{
    value: '1985',
}, {
    value: '1986',
}, {
    value: '1987',
}, {
    value: '1988',
}, {
    value: '1989',
}, {
    value: '1990',
}, {
    value: '1991',
}, {
    value: '1992',
}, {
    value: '1993',
}, {
    value: '1994',
}, {
    value: '1995',
}, {
    value: '1996',
}, {
    value: '1997',
}, {
    value: '1998',
}, {
    value: '1999',
}, {
    value: '2000',
}, {
    value: '2001',
}, {
    value: '2002',
}, {
    value: '2003',
}, {
    value: '2004',
}, {
    value: '2005',
}, {
    value: '2006',
}, {
    value: '2007',
}, {
    value: '2008',
}, {
    value: '2009',
}, {
    value: '2010',
}, {
    value: '2011',
}, {
    value: '2012',
}, {
    value: '2013',
}, {
    value: '2014',
}, {
    value: '2015',
}, {
    value: '2016',
}, {
    value: '2017',
}, {
    value: '2018',
}, {
    value: '2019',
}, {
    value: '2020',
}, {
    value: '2021',
}, {
    value: '2022',
}, {
    value: '2023',
}, {
    value: '2024',
}, {
    value: '2025',
}, {
    value: '2026',
}, {
    value: '2027',
}

];
// let min_price_range = [{
//     value: '$ 10000',
// }, {
//     value: '$ 15001',
// },
// {
//     value: '$ 20001',
// },
// {
//     value: '$ 25001',
// },
// {
//     value: '$ 18000',
// }
// ];
let property_types = [{
    value: 'Single-Family Homes',
}, {
    value: 'Condominium',
}, {
    value: 'Townhouse',
},
{
    value: 'Multi-Family Home',
}
];
let Bathroom = [{
    value: '1',
}, {
    value: '2',
},
{
    value: '3',
},
{
    value: '4',
}
    ,
{
    value: '5',
}
    ,
{
    value: '6',
}
    ,
{
    value: '7',
}
    ,
{
    value: '8',
}
    ,
{
    value: '9',
}
    ,
{
    value: '10',
}
    ,
{
    value: '11',
}
    ,
{
    value: '12',
}
];
class TransactionSeller extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hidePassword: true,
            secureTextEntry: true,
            isLoading: false,
            timeValue: '',
            location: '',
            city: '',
            statetext: '',
            zipcode: '',
            lotSize: '',
            no_bedrooms: '',
            no_bathrooms: '',
            no_parking_spaces: '',
            year_built: '',
            gradeSqft: '',
            mainsqFoot: '',
            basementTotal: '',
            basementfinished: '',
            minPrice: '',
            maxPrice: '',
            additionalcomments: '',
            propertytype: '',
            min_price_range: [],
            max_price_range: [],
            fullName: '',
            Photo: '',

        };
    }
    componentDidMount() {
        AsyncStorage.getItem('Id')
            .then((value) => {
                this.state.RoleId = value

                fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/ViewProfile', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "user_id": this.state.RoleId,
                    })
                }).then((response) => response.json())
                    .then((responseData) => {
                        if (responseData.status === "Success") {
                            this.state.transactionArry = responseData.data
                            this.state.profileimag = this.state.transactionArry[0].photo
                            AsyncStorage.setItem('Photo', this.state.transactionArry[0].photo);
                            AsyncStorage.setItem('fullName', fullName);
                            console.log("fname:", this.state.profileimag)

                        }
                        else {
                            //this.props.navigation.navigate('WelcomeScreen')
                        }
                        this.setState({
                            isFetching: false,
                        }, function () {
                        });
                    })

            

        });
        setInterval(() => {
            this.setState(() => {
                console.log(this.state.location);
                return { unseen: "does not display" }
            });
        }, 1000);
    }
    componentWillMount() {
        fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/settings', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then((response) => response.json())
            .then((responseData) => {
                this.setState({ isLoading: false });
                console.log("settings", responseData[0].min_price_range)
                this.state.min_price_range = responseData[0].min_price_range
                this.state.max_price_range = responseData[0].max_price_range
              
            })
            .catch((error) => {
                console.error(error);
            })
        }
        AddLoaction = () => {
            RNGooglePlaces.openAutocompleteModal("google")
                .then((place) => {
                    this.state.location = place.addressComponents[0].name
                    console.log("nbnbn  place", place.addressComponents);
                    this.state.city = place.addressComponents[1].name
                    this.state.statetext = place.addressComponents[2].name
                })
                .catch(error => console.log(error.message));
        }
        
    setTimeValue = (value) => {
        this.state.timeValue = value
        console.log("this.state.timeValue ", this.state.timeValue)
    }
    
    saveDetails() {
        if (this.state.bedroomsText == '' || this.state.bathroomText == ''
            || this.state.propertytype == '' || this.state.parkingno == ''
            || this.state.YearBuilt == '' || this.state.minprice == '' || this.state.maxprice == ''
            || this.state.additionalcomments == '' || this.state.lotSize == ''
            || this.state.gradeSqft == '' || this.state.mainsqFoot == '' || this.state.location == ''
            || this.state.city == '' || this.state.statetext == '' || this.state.zipcode == '' || this.state.basementTotal == ''
            || this.state.basementfinished == '') {
            Alert.alert("Please Enter All the Values.");
        }
        else {
            AsyncStorage.getItem('Id')
                .then((value) => {
                    this.state.userId = value
                    console.log("userId", this.state.userId)
                    fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/CreateTransaction', {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            "user_id": this.state.userId,
                            "role_id": "2",
                            "no_bedrooms": this.state.bedroomsText,
                            "no_bathrooms": this.state.bathroomText,
                            "property_type": this.state.propertytype,
                            "no_parking_spaces": this.state.parkingno,
                            "year_built": this.state.YearBuilt,
                            "listing_price_min": this.state.minprice,
                            "listing_price_max": this.state.maxprice,
                            "additional_comments": this.state.additionalcomments,
                            "lot_size": this.state.lotSize,
                            "builtup_area_grade": this.state.gradeSqft,
                            "builtup_area_main": this.state.mainsqFoot,
                            "property_address": this.state.location,
                            "city": this.state.city,
                            "state": this.state.statetext,
                            "zipcode": this.state.zipcode,
                            "basement_total": this.state.basementTotal,
                            "basement_finished": this.state.basementfinished

                        })
                    }).then((response) => response.json())
                        .then((responseData) => {
                            this.setState({ isLoading: false });
                            if (responseData.status === "Success") {
                                Alert.alert(JSON.stringify(responseData.message));
                                this.props.navigation.navigate('Dashboard', {})
                            }
                            else {

                            }

                        })
                        .catch((error) => {
                            console.error(error);
                        })
                })

        }
    }
    render() {

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 1, borderBottomColor: '#F1803A', backgroundColor: '#444444' }}>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ExistingTransaction')}>
                        <View style={{ top: 50, height: 50, width: 50, paddingLeft: 20 }}>
                            <Image source={require('../Images/profileback.png')} resizeMode="cover" />
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={{ marginTop: 20, alignSelf: 'center' }}>
                        <Image style={{ height: 80, width: 200 }} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
                    </View>

                    <View style={{ top: 50, height: 50, width: 50, }}>

                    </View>

                </View>
                <ScrollView
                    style={{ flex: 1, backgroundColor: '#444444' }}
                       >
                  <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={keyboardVerticalOffset}>

                    <View style={{ alignItems: 'center' }}>
                  
                                <Image style={{ height: 100, width: 100, borderRadius: 50, marginTop: 20 }}
                                    source={{ uri: this.state.profileimag }} />
                            
                    </View>
                    <TouchableOpacity>
                        <View style={{ alignItems: 'center' }}>
                            <Image style={{ height: 30, width: 30, borderRadius: 50, marginTop: -25, marginLeft: 80 }}
                                source={require('../Images/profilecam.png')} />
                        </View>
                    </TouchableOpacity>
                    <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 20, color: '#fff' }}>
                    {this.state.fullName}
                                </Text>

                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select Property Types'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 18 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={property_types}
                        onChangeText={propertytype => this.setState({ propertytype })}
                        value={this.state.propertytype}
                    />
                     <TouchableOpacity onPress={this.AddLoaction} >
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                marginLeft: 4, marginRight: 5, borderBottomColor: 'grey', borderBottomWidth: 1
                            }}>
                                <View style={{ flexDirection: 'row', height: 60, }}>
                                    <Text style={{ color: '#fff', fontSize: 17, textAlign: 'center', marginTop: 15 }} > {"Enter a location"} </Text>
                                </View>
                                <Text style={{ color: '#fff', marginTop: 15, fontSize: 17 }} > {this.state.location}</Text>
                            </View>
                        </TouchableOpacity>
                    <Input
                        autoCapitalize='none'
                        onChangeText={city => this.setState({ city })}
                        inputStyle={{ height: 60, color: '#fff', fontSize: 17 }}
                        placeholderTextColor="#fff"
                        placeholder='City'
                        value={this.state.city}

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={statetext => this.setState({ statetext })}
                        value={this.state.statetext}
                        inputStyle={{ height: 60, color: '#fff', fontSize: 17 }}
                        placeholderTextColor="#fff"
                        placeholder='State'

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={zipcode => this.setState({ zipcode })}
                        value={this.state.zipcode}
                        maxLength={5}
                        keyboardType={'numeric'}
                        inputStyle={{ height: 60, color: '#fff', fontSize: 17 }}
                        placeholderTextColor="#fff"
                        placeholder='Zipcode'

                    />
                    <Input
                        autoCapitalize='none'
                        inputStyle={{ height: 60, color: '#fff', fontSize: 17 }}
                        placeholderTextColor="#fff"
                        placeholder='Lot Size'
                        onChangeText={lotSize => this.setState({ lotSize })}
                        value={this.state.lotSize}

                    />
<Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select Min Price'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={this.state.min_price_range}
                        onChangeText={minPrice => this.setState({ minPrice })}
                        value={this.state.minPrice}
                    />

                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select Max Price'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={this.state.max_price_range}
                        onChangeText={maxPrice => this.setState({ maxPrice })}
                        value={this.state.maxPrice}
                    />


                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select No. of Bedrooms'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={Bathroom}
                        onChangeText={no_bedrooms => this.setState({ no_bedrooms })}
                        value={this.state.no_bedrooms}
                    />


                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select No. of Bathroom'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={Bathroom}
                        onChangeText={no_bathrooms => this.setState({ no_bathrooms })}
                        value={this.state.no_bathrooms}
                    />


                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select No. of Parking'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={Bathroom}
                        onChangeText={no_parking_spaces => this.setState({ no_parking_spaces })}
                        value={this.state.no_parking_spaces}
                    />
                    
                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select Year Built'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={YearBuilt}
                        onChangeText={year_built => this.setState({ year_built })}
                        value={this.state.year_built}
                    />

                    <Input
                        autoCapitalize='none'
                        onChangeText={gradeSqft => this.setState({ gradeSqft })}
                        value={this.state.gradeSqft}
                        inputStyle={{ height: 60, color: '#fff',fontSize: 17 }}
                        placeholderTextColor="#fff"
                        keyboardType={'numeric'}
                        placeholder='Above Grade sqft'

                    />

                    <Input
                        autoCapitalize='none'
                        onChangeText={mainsqFoot => this.setState({ mainsqFoot })}
                        value={this.state.mainsqFoot}
                        inputStyle={{ height: 60, color: '#fff',fontSize: 17 }}
                        placeholderTextColor="#fff"
                        keyboardType={'numeric'}
                        placeholder='Mainsq Footage'

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={basementTotal => this.setState({ basementTotal })}
                        value={this.state.basementTotal}
                        inputStyle={{ height: 60, color: '#fff', fontSize: 17 }}
                        placeholderTextColor="#fff"
                        keyboardType={'numeric'}
                        placeholder='Basement Total'

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={basementfinished => this.setState({ basementfinished })}
                        value={this.state.basementfinished}
                        inputStyle={{ height: 60, color: '#fff', fontSize: 17 }}
                        placeholderTextColor="#fff"
                        keyboardType={'numeric'}
                        placeholder='Basement Finished'

                    />
                  
                    
                    <TextInput style={{ height: 100, margin: 20,fontSize: 17, borderColor: 'gray', borderWidth: 1, borderRadius: 20, color: 'white', paddingLeft: 20 }}
                        onChangeText={additionalcomments => this.setState({ additionalcomments })}
                        value={this.state.additionalcomments}

                    />
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10 }}>
                        <TouchableOpacity onPress={() => this.saveDetails()} style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }}>
                            <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10 }}>
                                Create Transaction
                            </Text>
                        </TouchableOpacity>
                    </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>


        );
    }
}

export default TransactionSeller;

