import React, { PureComponent } from 'react';
import {
  Image, Modal, ScrollView, TouchableWithoutFeedback, FlatList, View, Text, Dimensions,ActivityIndicator, TouchableOpacity, TouchableHighlight,AsyncStorage, Alert
} from 'react-native';
import { Input } from 'react-native-elements'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
let styles = {

  fixToTextone: {
    marginTop: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 96,
  },
}

export default class MarketPlace extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalAcceptVisible: false,
      modalCounterVisible:false,
      marketplaceArry: [],
      message:'',
      uId:'',
      marketID:'',
      UsrId:'',
      isLoading: false

    }
  }
  setModalCounterVisible(visible){
    this.setState({ modalCounterVisible: visible });

  }
  setModalVisible(visible) {
    this.setState({ modalAcceptVisible: visible });
  }
  _onPresscloseButton(){
    this.setModalCounterVisible(!this.state.modalCounterVisible);
  }
  _onPressCounterButton(){
    //this.setModalCounterVisible(!this.state.modalCounterVisible);
    AsyncStorage.getItem('Id')
    .then((value) => {
      this.state.UsrId = value
    fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/Counter', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "user_from": this.state.UsrId,//login
          "marketplace_id": this.state.marketID,
          "user_to":this.state.uId,
          "pay_amount":this.state.message
        })
      }).then((response) => response.json())
        .then((responseData) => {
          if (responseData.status === "Success") {
            this.MarketplaceAPI()
            this.setModalCounterVisible(!this.state.modalCounterVisible);
           Alert.alert(responseData.message)
  
          }
          else {
            //this.props.navigation.navigate('WelcomeScreen')
          }
          this.setState({
            isFetching: false,
          }, function () {
          });
        })
      })
  }
  _onPressAcceptButton(){
         fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/Accept', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "user_id": this.state.uId,
          "marketplace_id": this.state.marketID
        })
      }).then((response) => response.json())
        .then((responseData) => {
          if (responseData.status === "Success") {
            this.MarketplaceAPI()
           Alert.alert(responseData.message)
           this.setModalCounterVisible(!this.state.modalAcceptVisible);
          }
          else {
            //this.props.navigation.navigate('WelcomeScreen')
          }
          this.setState({
            isFetching: false,
          }, function () {
          });
        })
        
   
  }
  _onPressMoreButton() {
    this.setModalVisible(!this.state.modalAcceptVisible);
  }
  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#000",
        }}
      />
    );
  }
  MarketplaceAPI(){
    this.setState({ isLoading: true })
    AsyncStorage.getItem('Id')
    .then((value) => {
      this.state.UsrId = value

    fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/Marketplace', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "user_id": this.state.UsrId,
      })
    }).then((response) => response.json())
      .then((responseData) => {
        if (responseData.status === "Success") {
          this.setState({ isLoading: false })
          this.state.marketplaceArry = responseData.data
          console.log("marketplaceArry:", this.state.marketplaceArry)

        }
        else {
          this.setState({ isLoading: false })
          //this.props.navigation.navigate('WelcomeScreen')
        }
        this.setState({
          isFetching: false,
        }, function () {
        });
      })
    })
  }
  componentWillMount() {
   this.MarketplaceAPI()

  }
  hideAccept(item){
    if(this.state.UsrId == item.user_id){
      if(item.own_listing == false){
        return<View style={{
          fontSize: 20, fontSize: 16, marginTop: 7, width: 150,
          height: 40, alignItems: 'center',
          justifyContent: 'center', 
        }}>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Text style={{ textAlign: 'center', color: "#fff", fontSize: 15, paddingLeft: 10,fontWeight: 'bold' }}>
            My Listing 
                              </Text>
          </View>
        </View>
        
        
        }
    }else{
  return<TouchableOpacity onPress={() => {
    this.setModalVisible(true);
    this.state.marketID = item.id
    this.state.uId = item.user_id
  }}>
    <View style={{
      fontSize: 20, fontSize: 16,
      height: 40, alignItems: 'center', marginTop: 5, width: 150,
      justifyContent: 'center', backgroundColor: '#63D436', borderRadius: 25,
    }}>

      <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
        <Image style={{ height: 25, width: 25, borderRadius: 50, }}
          source={require('../Images/accept.png')} />
        <Text style={{ textAlign: 'center', color: "#fff", fontSize: 15, paddingLeft: 10,fontWeight: 'bold',marginTop:2 }}>
          Accept
         </Text>
      </View>
    </View>
  </TouchableOpacity>
    }
}
  hideCounter(item){
    if(this.state.UsrId == item.user_id){
      if(item.own_listing == false){
        return<View style={{
          fontSize: 20, fontSize: 16, marginTop: 7, width: 150,
          height: 40, alignItems: 'center',
          justifyContent: 'center', 
        }}>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Text style={{ textAlign: 'center', color: "#fff", fontSize: 15, paddingLeft: 10,fontWeight: 'bold' }}>
            My Listing 
                              </Text>
          </View>
        </View>
        
        
        }
    }else{
if (item.you_countered == true){
return<View style={{
  fontSize: 20, fontSize: 16, marginTop: 7, width: 150,
  height: 40, alignItems: 'center',
  justifyContent: 'center', 
}}>
  <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
    <Text style={{ textAlign: 'center', color: "#fff", fontSize: 15,fontWeight: 'bold', paddingLeft: 10 }}>
    You Countered
                      </Text>
  </View>
</View>
  }else{
    return<TouchableOpacity onPress={() => {
      this.setModalCounterVisible(true);
      this.state.marketID = item.id
      this.state.uId = item.user_id
    }}>
      <View style={{
        fontSize: 20, fontSize: 16, marginTop: 7, width: 150,
        height: 40, alignItems: 'center',
        justifyContent: 'center', backgroundColor: '#D2D744', borderRadius: 25,
      }}>
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <Image style={{ height: 25, width: 25, borderRadius: 50, }}
            source={require('../Images/counter.png')} />
          <Text style={{ textAlign: 'center', color: "#fff", fontSize: 15,fontWeight: 'bold', paddingLeft: 10,marginTop:2 }}>
            Counter
                            </Text>
        </View>
      </View>
    </TouchableOpacity>
  }
}
  }
  keyExtractor = (item, index) => index.toString()

  renderItem = ({ item }) => (
    <View>
      <View style={styles.fixToTextone}>
        <Text style={{ paddingLeft: 15, textShadowColor: 'black', color: '#B18556', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: 40 }}>{item.id}</Text>

        <View style={{ height: 95, width: 95, alignItems: 'center', borderLeftWidth: 1 }}>
          <Image style={{ height: 70, width: 70, marginTop: 5 }} source={require('../Images/newshapea.png')} />
          <Text style={{ textShadowColor: 'black', color: '#000', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: -45 }}>{item.points_offered}</Text>
        </View>
        <View style={{ height: 95, width: 95, alignItems: 'center', borderLeftWidth: 1 }}>
          <Image style={{ height: 70, width: 70, marginTop: 5 }} source={require('../Images/newshapeb.png')} />
          <Text style={{ textShadowColor: 'black', color: '#000', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: -45 }}>{"$"+ item.face_value}</Text>
        </View>

        <View style={{ height: 95, width: 95, alignItems: 'center', borderLeftWidth: 1 }}>
          <Image style={{ height: 70, width: 70, marginTop: 5 }} source={require('../Images/newshapeb.png')} />
          <Text style={{ textShadowColor: 'black', color: '#000', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: -45 }}>{"$"+ item.offer_price}</Text>
        </View>
      </View>
      <View style={{ marginTop: 2, height: 60, flexDirection: 'row', justifyContent: 'space-around', paddingLeft: 60 }}>
     
        {this.hideAccept(item)}
        {this.hideCounter(item)}
        
      </View>
    </View>

  )
  FlatListHeader = () => {
    return (
      <View elevation={1}
        style={{
          flexDirection: 'row',
          height: 65,
          margin: 1,
          backgroundColor: "#444444",
          borderBottomWidth: 1,
          alignSelf: "center",


        }}
      >
        <Text style={{ paddingLeft: 15, textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, marginTop: 10, fontWeight: 'bold', height: 60 }}>Transaction id</Text>
        <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, marginTop: 10, fontWeight: 'bold', height: 60 }}>Points Offered</Text>
        <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, marginTop: 10, fontWeight: 'bold', height: 60 }}>Face Value</Text>
        <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, marginTop: 10, fontWeight: 'bold', height: 60 }}>Offer Price</Text>
      </View>
    );
  }
  render() {

    return (

      <View style={{ flex: 1, backgroundColor: "#444444" }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Dashboard')}>
            <View style={{ top: 50, height: 50, width: 50, paddingLeft: 20 }}>
              <Image style={{ top: 20 }} source={require('../Images/back.png')} resizeMode="cover" />
            </View>
          </TouchableWithoutFeedback>
          <View style={{ marginTop: 26, alignSelf: 'center' }}>
            <Image style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
          </View>
          <TouchableWithoutFeedback >
            <View style={{ top: 50, height: 50, width: 50, }}>

              <Image style={{ top: 14 }} source={require('../Images/marketplacefilter.png')} resizeMode="cover" />

            </View>
          </TouchableWithoutFeedback>
        </View>
        <ScrollView
          style={{
            flex: 1,
            backgroundColor: '#444444'
          }}
          contentContainerStyle={styles.contentContainer}
          keyboardShouldPersistTaps='handled'
        >

          <View style={{ backgroundColor: "#444444", marginTop: 2, borderTopWidth: 1, borderTopColor: "#E9883A" }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <Image style={{ height: 25, width: 25, borderRadius: 50, marginTop: 20 }}
                source={require('../Images/mpicoon.png')} />
              <Text style={{ textAlign: 'center', marginTop: 20, color: "#E9883A", fontSize: 20, paddingLeft: 10 }}>
                Market Place
                                </Text>
            </View>
            <View style={{ backgroundColor: "#444444", }}>
              
              <FlatList style={{ marginTop: 10 }}
                keyExtractor={this.keyExtractor}
                ListHeaderComponent={this.FlatListHeader}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                data={this.state.marketplaceArry}
                renderItem={this.renderItem}
              />
  < ActivityIndicator style={{ position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'}} size="large" color="#E9883A" animating={this.state.isLoading} />
              <Modal style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                padding: 10,
              }}
                //animationType="slide"
                transparent={true}
                visible={this.state.modalAcceptVisible}
                onRequestClose={() => {
                  alert('Modal has been closed.');
                }}>
                <View style={{ marginTop:Platform.OS === 'ios' ? 100 : 30, backgroundColor: '#444444', height: 160, borderColor: '#fff', borderWidth: 1, margin: 20 }}>
                  <TouchableHighlight
                    onPress={() => {
                      this.setModalVisible(!this.state.modalAcceptVisible);
                    }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#E9883A'}}>
                      <Text style={{ height: 30, width: 200, color: '#fff', fontWeight: 'bold', borderRadius: 15, marginTop: 10, paddingLeft: 10,fontSize:20 }}>ACCEPT</Text>
                      <Text style={{ height: 30, width: 30, color: '#fff', borderRadius: 15, marginTop: 17, paddingLeft: 10 ,fontWeight: 'bold'}}>X</Text>
                    </View>
                  </TouchableHighlight>
                  <Text style={{ height: 30, color: '#fff', fontWeight: 'bold', borderRadius: 15, marginTop: 17, paddingLeft: 10 }}>Are you sure you want to accept?</Text>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                      <TouchableOpacity onPress={() => this._onPressMoreButton()} style={{ backgroundColor: '#000', height: 40, width: 140, borderRadius: 5 }} >
                        <Text style={{ color: '#fff', textAlign: 'center', marginTop: 8, fontSize: 18 }}>
                          Close
                            </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                      <TouchableOpacity onPress={() => this._onPressAcceptButton()} style={{ backgroundColor: '#E9883A', height: 40, width: 140, borderRadius: 5 }} >
                        <Text style={{ color: '#fff', textAlign: 'center', marginTop: 8, fontSize: 18 }}>
                          Accept
                            </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>

              <Modal style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                padding: 10,
              }}
                //animationType="slide"
                transparent={true}
                visible={this.state.modalCounterVisible}
                onRequestClose={() => {
                  alert('Modal has been closed.');
                }}>
                <View style={{ marginTop: Platform.OS === 'ios' ? 100 : 30, backgroundColor: '#444444', height: 160, borderColor: '#fff',  borderWidth: 1, margin: 20 }}>
                  <TouchableHighlight
                    onPress={() => {
                      this.setModalCounterVisible(!this.state.modalCounterVisible);
                    }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#45B5F8',  }}>

                      <Text style={{ height: 30, width: 200, color: '#fff', fontWeight: 'bold', borderRadius: 15, marginTop: 10, paddingLeft: 10,fontSize:20 }}>Counter</Text>

                      <Text style={{ height: 30, width: 30, color: '#fff', borderRadius: 15, marginTop: 17, paddingLeft: 10,fontWeight: 'bold' }}>X</Text>

                    </View>
                  </TouchableHighlight>
                  <Input
                  onChangeText={message => this.setState({ message })}
                  keyboardType="number-pad"
                  autoCapitalize='sentences'
                  placeholder='Enter you want to pay'
                  inputStyle={{ height: 50, color: '#fff' }}
                  placeholderTextColor="#fff"
                  
                />


                  <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                      <TouchableOpacity onPress={() => this._onPresscloseButton()} style={{ backgroundColor: '#000', height: 40, width: 140, borderRadius: 5 }} >
                        <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 18 }}>
                          Close
                            </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                      <TouchableOpacity onPress={() => this._onPressCounterButton()} style={{ backgroundColor: '#45B5F8', height: 40, width: 140, borderRadius: 5 }} >
                        <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 18 }}>
                          Counter
                            </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>
           
            </View>
           
          </View>
        
        </ScrollView>
      </View>


    );
  }
}


