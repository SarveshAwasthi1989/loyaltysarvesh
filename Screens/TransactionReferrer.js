import { TextInput } from 'react-native-gesture-handler';
import React from 'react';
import {
    Image, Dimensions, TouchableOpacity, ScrollView, View, Text,Alert, TouchableWithoutFeedback, KeyboardAvoidingView,AsyncStorage
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { Header, Input } from 'react-native-elements'
 import { Dropdown } from 'react-native-material-dropdown';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const keyboardVerticalOffset = Platform.OS === 'ios' ? 10 : 0
import { TextInputMask } from 'react-native-masked-text'
let RefereeUser = [{
    value: 'Buyer',
}, {
    value: 'Seller',
}, {
    value: 'Referrer',
}, {
    value: 'Landlord',
}
];
class TransactionReferrer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fullName:'',
            Photo:'',
            userId:'',
            referee_first_name:'',
            referee_last_name:'',
            referee_email:'',
            referee_phone:'',
            usertype:'',
            additionalcomments:''

        };

    }
    componentDidMount() {
        AsyncStorage.getItem('Id')
    .then((value) => {
      this.state.userId = value
    
      fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/ViewProfile', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "user_id": this.state.userId,
        })
      }).then((response) => response.json())
        .then((responseData) => {
          if (responseData.status === "Success") {
            this.state.transactionArry = responseData.data
            let fname = this.state.transactionArry[0].first_name
      
              var lanme = this.state.transactionArry[0].last_name
              this.state.fullName = fname.concat(" " , lanme);
            this.state.Photo = this.state.transactionArry[0].photo
  
          }
          else {
            //this.props.navigation.navigate('WelcomeScreen')
          }
          this.setState({
            isFetching: false,
          }, function () {
          });
        })
        
    });
    }
    setTimeValue = (value) => {
        this.state.timeValue = value
        console.log("this.state.timeValue ", this.state.timeValue)
    }
    saveDetails() {
        if(this.state.referee_first_name == '' || this.state.referee_last_name == '' 
        || this.state.referee_email == '' || this.state.referee_phone == ''|| this.state.usertype == ''
        || this.state.additionalcomments == '' ){
            Alert.alert("Please Enter All the Values.");
          }
          else{
          AsyncStorage.getItem('Id')
          .then((value) => {
            this.state.userId = value
            console.log("userId",this.state.userId)
            fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/CreateTransaction', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "user_id": this.state.userId,
              "role_id": "4",
              "referee_first_name": this.state.referee_first_name,
              "referee_last_name":this.state.referee_last_name,
              "referee_email":this.state.referee_email,
              "referee_phone":this.state.referee_phone,
              "referee_user_type":this.state.usertype,

             
            })
          }).then((response) => response.json())
            .then((responseData) => {
              this.setState({ isLoading: false });
              console.log("ffy",responseData)
              if (responseData.status === "Success") {
                Alert.alert(JSON.stringify(responseData.message));
                this.props.navigation.navigate('Dashboard', {})
              }
              else{
    
              }
            
            })
            .catch((error) => {
              console.error(error);
            })
        })
      
    }
        }
    render() {

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between',borderBottomWidth:1,borderBottomColor:'#F1803A' ,backgroundColor:'#444444'}}>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ExistingTransaction')}>
                <View style={{ top: 50, height: 50, width: 50, paddingLeft: 20 }}>
            
                            <Image source={require('../Images/profileback.png')} resizeMode="cover" />
                        </View>
                    </TouchableWithoutFeedback>
            <View style={{ marginTop: 20, alignSelf: 'center' }}>
              <Image style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
            </View>
            
              <View style={{ top: 50,  height: 50, width: 50, }}>

              </View>
            
          </View>
               
                <ScrollView
                    style={{ flex: 1, backgroundColor: '#444444' }}
                    
                >
                    <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={keyboardVerticalOffset}>

                    <View style={{ alignItems: 'center' }}>
                        <Image style={{ height: 100, width: 100, borderRadius: 50, marginTop: 20 }}
                           source={{ uri: this.state.Photo }} />
                    </View>
                    <TouchableOpacity>
                        <View style={{ alignItems: 'center' }}>
                            <Image style={{ height: 30, width: 30, borderRadius: 50, marginTop: -25, marginLeft: 80 }}
                                source={require('../Images/profilecam.png')} />
                        </View>
                    </TouchableOpacity>
                    <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 20, color: '#fff' }}>
                        {this.state.fullName}
                                </Text>

                    <Input
                        autoCapitalize='none'
                        onChangeText={referee_first_name => this.setState({ referee_first_name })}
                        inputStyle={{ height: 60, color: '#fff' }}
                        placeholderTextColor="#fff"
                        placeholder='Referee First Name'

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={referee_last_name => this.setState({ referee_last_name })}
                        inputStyle={{ height: 60, color: '#fff' }}
                        placeholderTextColor="#fff"
                        placeholder='Referee Last Name'

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={referee_email => this.setState({ referee_email })}
                        inputStyle={{ height: 60, color: '#fff' }}
                        placeholderTextColor="#fff"
                        placeholder='Referee Email'

                    />
                    {/* <Input
                        autoCapitalize='none'
                        onChangeText={referee_phone => this.setState({ referee_phone })}
                        inputStyle={{ height: 60, color: '#fff' }}
                        placeholderTextColor="#fff"
                        placeholder='Referee Phone number'

                    /> */}
                    <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: "grey", paddingRight: 90, height: 50 }}>

<TextInputMask
  type={'custom'}
  options={{
   
    mask: '999-999-9999'
  }}

  autoCapitalize='none'
  placeholder='Phone Number'
  placeholderTextColor="#fff"
  value={this.state.referee_phone}
  onChangeText={referee_phone => {
    this.setState({
      referee_phone
    })
  }}

  style={{
    height: 50,
    width: '80%',
    color: '#fff',
    paddingLeft: 10,
    fontSize: 18
  }}
/>
</View>



                  

                     <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select Referee User Type'
                        textColor={'#fff'} 
                        labelFontSize={18}
                        selectedItemColor={'#fff'} 
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 18 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={RefereeUser}
                        onChangeText={usertype => this.setState({ usertype })}
                        value={this.state.usertype}
                    /> 




<TextInput style={{ height: 100, margin: 10, borderColor: 'gray', borderWidth: 1, borderRadius: 20, color: 'white', paddingLeft: 20 }}
                            //onChangeText={additionalcomments => onChangeText(text)}
                            onChangeText={additionalcomments => this.setState({ additionalcomments })}
                            value={this.state.additionalcomments}

                        />
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10,marginTop:30 }}>
                        <TouchableOpacity onPress={() => this.saveDetails()} style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }}>
                            <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 18 }}>
                                Create Transaction
                            </Text>
                        </TouchableOpacity>
                    </View>
</KeyboardAvoidingView>
                </ScrollView>
            </View>


        );
    }
}

export default TransactionReferrer;

