
import React from 'react';
import {
  SafeAreaView, Dimensions, Alert,
  ScrollView, View, Image, Text, Platform, TouchableOpacity, TextInput,TouchableWithoutFeedback
} from 'react-native';
import {Header } from 'react-native-elements'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

let styles = {
  scroll: {
    flex: 1,
    backgroundColor: '#444444'
  },
  container: {
    marginTop: Platform.select({ ios: 70, android: 70 }),
    flex: 1,
    padding:20,
   
  },

  arrowbuttonContainer: {
    paddingTop: 8,
    margin: 8,
    flexDirection: 'row-reverse'
  },
  buttonContainer: {
    flex: 1,

  },
  textContainer: {
    textAlign: 'center', // <-- the magic
    fontSize: 20,
    marginTop: 60,
    width: width,
    justifyContent: 'flex-start',
  },

  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 20,
  },
  instaButton: {
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1D7AE5',
    borderRadius: 30,
    borderColor: "rgba(255,255,255,0.7)",
    margin: 20,
  },
  createaccountButton: {
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    borderRadius: 30,
    borderColor: "grey",
    //borderTopColor: '#1D7AE5',
    borderWidth: 1,
  },
  textaccountButton: {
    marginTop: 20,
    margin: 4,
  },
  safeContainer: {
    flex: 1,
  },
  touachableButton: {
    position: 'absolute',
    right: 3,
    height: 40,
    width: 35,
    padding: 2
  },
};

let defaults = {
  username: '',
  password: '',
};
class Addreferee extends React.Component {
  constructor(props) {
    super(props);
    this.onFocus = this.onFocus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitUserName = this.onSubmitUserName.bind(this);
    this.onSubmitPassword = this.onSubmitPassword.bind(this);
    this.usernameRef = this.updateRef.bind(this, 'username');
    this.passwordRef = this.updateRef.bind(this, 'password');
    this.renderPasswordAccessory = this.renderPasswordAccessory.bind(this);

    this.state = {
      hidePassword: true,
      secureTextEntry: true,
      isLoading: false,
      ...defaults,
    };
  }
  onFocus() {
    let { errors = {} } = this.state;
    for (let name in errors) {
      let ref = this[name];
      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }


  onChangeText(text) {
    ['username', 'password']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
        }
      });
  }
  onSubmitUserName() {
    this.password.focus();

  }

  onSubmitPassword() {
    this.password.focus();
  }
  onSubmit() {
    let errors = {};

    ['username', 'password']
      .forEach((name) => {
        let value = this[name].value();

        if (!value) {
          errors[name] = 'Should not be empty';
        } else {
          if ('password' === name && value.length < 6) {
            errors[name] = 'Too short';
          }
          else {
          }
        }
      });

    this.setState({ errors });
  }




  updateRef(name, ref) {
    this[name] = ref;
  }

  renderPasswordAccessory() {
    let { secureTextEntry } = this.state;

    let name = secureTextEntry ?
      'visibility' :
      'visibility-off';
  }

  handleforgotPress = () => {
  };
  handlebackPress = () => {
  };
  setPasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  }
  render() {
    let { errors = {}, secureTextEntry, ...data } = this.state;

    let { username, password } = data;
    let defaultEmail = `${username || ''}${password || ''}`
      .replace(/\s+/g, '_')
      .toLowerCase();

    return (
      <ScrollView
        style={styles.scroll}
        contentContainerStyle={styles.contentContainer}
        keyboardShouldPersistTaps='handled'
      >
   <Header
          statusBarProps={{ barStyle: 'light-content' }}
          barStyle="light-content" // or directly
          leftComponent={{  color: 'grey' }}
          centerComponent={<TouchableWithoutFeedback >
          <View >
          <Text >Add referee details </Text>
          </View>
        </TouchableWithoutFeedback>}
          containerStyle={{
            backgroundColor: '#444444',
            justifyContent: 'space-around',
          }}
        />
        
        <View style={styles.container}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
            <TextInput style={{ color: "balck", borderBottomWidth: 0.5, fontSize: 15, width: '40%',top:20,height:60,margin:6 }}
              placeholder="First Name"
              underlineColorAndroid = "transparent" 
              placeholderTextColor = "white"
              value={this.state.Email}
              onChangeText={Email => this.setState({ Email })} />
        <TextInput  style = { styles.textInput }></TextInput>

 <TextInput  style={{ color: "white", borderBottomWidth: 0.5, fontSize: 15, width: '40%', top: 20,height:60,margin:6, }}
              placeholder="Last Name"
              underlineColorAndroid = "transparent" 
              placeholderTextColor = "white"
              value={this.state.Email}
              onChangeText={Email => this.setState({ Email })} />     
                   </View>


          <View>
        

<TextInput style={{ color: "white", borderBottomWidth: 0.5, fontSize: 15, width: '100%', top: 30 ,height:60,}}
              placeholder="Email"
              underlineColorAndroid = "transparent" 
              placeholderTextColor = "white"
              value={this.state.Email}
              onChangeText={Email => this.setState({ Email })} />


<TextInput style={{ color: "white", borderBottomWidth: 0.5, fontSize: 15, width: '100%', top: 40,height:60 }}
              placeholder="Phone Number "
              underlineColorAndroid = "transparent" 
              placeholderTextColor = "white"
              value={this.state.Email}
              onChangeText={Email => this.setState({ Email })} />


<TouchableOpacity   onPress={() => this.props.navigation.navigate('Addreferee')} >
              <Text style={{
                textAlign: 'center',color:'white',
                fontSize: 20, fontSize: 16, padding: 15,
                 height: 50,margin:30, alignItems: 'center',top:40,
                  justifyContent: 'center',backgroundColor:'#F1803A'
               
              }} >Submit</Text>
            </TouchableOpacity>
            {/* <Button style={{fontSize: 10,top:70,width: 80,} }
             title="Login"
              onPress={() => this.props.navigation.navigate('MainPage', {  })}
                />  */}
           




          </View>
        </View>


      </ScrollView>
    );
  }
}

export default Addreferee;