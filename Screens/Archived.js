import React, { PureComponent } from 'react';
import {
    Image, SafeAreaView, StyleSheet,TouchableWithoutFeedback, FlatList, View, Text, ActivityIndicator,Dimensions,TouchableOpacity
} from 'react-native';
import { ListItem ,Header} from 'react-native-elements'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
console.disableYellowBox = true;

let styles = {

    fixToTextone: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      height: 40,
    },
}
const list = [
    // {
    //   name: 'ShreeBuy..',
    //   type: 'Buyer',
    //   status: 'invited',
    //   points: '100',
    //   archive: ''
    // },
    // {
    //     name: 'ShreeBuy..',
    //     type: 'Buyer',
    //     status: 'invited',
    //     points: '100',
    //     archive: ''
    //   },
    //   {
    //     name: 'ShreeBuy..',
    //     type: 'Buyer',
    //     status: 'Active',
    //     points: '100',
    //     archive: ''
    //   },
    //   {
    //     name: 'ShreeBuy..',
    //     type: 'Buyer',
    //     status: 'invited',
    //     points: '100',
    //     archive: ''
    //   }, {
    //     name: 'ShreeBuy..',
    //     type: 'Buyer',
    //     status: 'invited',
    //     points: '100',
    //     archive: ''
    //   }, {
    //     name: 'ShreeBuy..',
    //     type: 'Buyer',
    //     status: 'invited',
    //     points: '100',
    //     archive: ''
    //   }, {
    //     name: 'ShreeBuy..',
    //     type: 'Buyer',
    //     status: 'invited',
    //     points: '100',
    //     archive: ''
    //   },
    //   {
    //     name: 'ShreeBuy..',
    //     type: 'Buyer',
    //     status: 'invited',
    //     points: '100',
    //     archive: ''
    //   }, {
    //     name: 'ShreeBuy..',
    //     type: 'Buyer',
    //     status: 'invited',
    //     points: '100',
    //     archive: ''
    //   }, {
    //     name: 'ShreeBuy..',
    //     type: 'Buyer',
    //     status: 'invited',
    //     points: '100',
    //     archive: ''
    //   },
    //    {
    //     name: 'ShreeBuy..',
    //     type: 'Buyer',
    //     status: 'invited',
    //     points: '100',
    //     archive: ''
    //   },
      
  ]
  export default class Archived extends PureComponent {



    keyExtractor = (item, index) => index.toString()

    renderItem = ({ item }) => (
      <View style={styles.fixToTextone}>
         <Text style={{ paddingLeft:15, textShadowColor: '#fff',color:'#fff',   flex: 1, alignSelf: "center",  fontSize: 15,height: 40,}}>{item.name}</Text>
            <Text style={{  textShadowColor: '#fff', color:'#fff',   flex: 1, alignSelf: "center",  fontSize: 15,height: 40,}}>{item.type}</Text>
            <Text style={{  textShadowColor: '#fff',  color:'#fff',   flex: 1, alignSelf: "center",  fontSize: 15,height: 40,}}>{item.status}</Text>
            <Text style={{  textShadowColor: '#fff',  color:'#fff',   flex: 1, alignSelf: "center", fontSize: 15,height: 40,}}>{item.points}</Text>
      </View>
      
       
    )
    FlatListHeader = () => {
        return (
          <View elevation={1} 
            style={{
                flexDirection: 'row',
              height: 40,
              margin: 1,
              backgroundColor: "#444444",
              borderColor: "black",
              alignSelf: "center",
             
              shadowOpacity: 1,
              
            }}
          >
            <Text style={{ paddingLeft:15, textShadowColor: 'black',color:'#fff',   flex: 1, alignSelf: "center",  fontSize: 15}}>Name</Text>
            <Text style={{  textShadowColor: 'black', color:'#fff',   flex: 1, alignSelf: "center",  fontSize: 15}}>Type</Text>
            <Text style={{  textShadowColor: 'black',  color:'#fff',   flex: 1, alignSelf: "center",  fontSize: 15}}>Status</Text>
            <Text style={{  textShadowColor: 'black',  color:'#fff',   flex: 1, alignSelf: "center", fontSize: 15}}>Points</Text>
          </View>
        );
      }
     
    render() {

        return (

            <View style={{ flex: 1,backgroundColor: "#444444",}}>
 <View style={{ backgroundColor: "#444444",}}>
               
                                <View style={{backgroundColor: "#444444",}}>
                                <View elevation={1} 
            style={{
                flexDirection: 'row',
              height: 40,
              margin: 1,
              backgroundColor: "#444444",
              alignSelf: "center",
              borderBottomWidth:0.5,
              borderBottomColor:'#E9883A'
             
              
            }}
          >
            <Text style={{ paddingLeft:15, textShadowColor: 'black',color:'#fff',   flex: 1, alignSelf: "center",  fontSize: 15}}>Name</Text>
            <Text style={{  textShadowColor: 'black', color:'#fff',   flex: 1, alignSelf: "center",  fontSize: 15}}>Type</Text>
            <Text style={{  textShadowColor: 'black',  color:'#fff',   flex: 1, alignSelf: "center",  fontSize: 15}}>Status</Text>
            <Text style={{  textShadowColor: 'black',  color:'#fff',   flex: 1, alignSelf: "center", fontSize: 15}}>Points</Text>
            <Text style={{  textShadowColor: 'black', color:'#fff',   flex: 1, alignSelf: "center",  fontSize: 15}}>Archive</Text>
          </View>
                <FlatList style={{top:10,height: height-400,}}
      keyExtractor={this.keyExtractor}
      data={list}
      renderItem={this.renderItem}
    />
     <View style={{ alignContent: 'center', alignItems: 'center',bottom:10 }}>
                                <TouchableOpacity style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }}>
                                    <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10 ,fontSize:18}}>
                                        Add New Transaction
                            </Text>
                                </TouchableOpacity>
                            </View>
                </View>
            </View>
    </View>
   

        );
    }
}


