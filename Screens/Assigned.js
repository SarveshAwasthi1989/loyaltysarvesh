import React, { PureComponent } from 'react';
import {
  RefreshControl,Image, TouchableWithoutFeedback, Modal, ScrollView, Alert, TouchableHighlight, AsyncStorage, FlatList, View, Text, ActivityIndicator, Dimensions, TouchableOpacity,Platform
} from 'react-native';
import { ListItem, Header } from 'react-native-elements'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { Input } from 'react-native-elements'
console.disableYellowBox = true;

let styles = {

  fixToTextone: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 70,
    
  },
}

export default class Assigned extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      RoleId: '',
      pointArry: [],
      total_assigned_points: '',
      total_vested_points: '',
      usable_vested_points: '',
      pointView: '',
      facevalue: '',
      modalRedeemVisible: false,
      modalTradeVisible: false,
      message: '',
      userId: '',
      TradePoint: '',
      TradeValue: '',
      isLoading: false,
      FaceValue: ''

    }
  }
  setModalTradeVisible(visible) {
    this.setState({ modalRedeemVisible: visible });

  }
  setModalVisible(visible) {
    this.setState({ modalTradeVisible: visible });
  }
  _onPressRedeemButton() {
    if (this.state.message == '') {
      Alert.alert("not blank")
    }
    else {
      AsyncStorage.getItem('Id')
        .then((value) => {
          this.state.userId = value
          console.log("userId", this.state.userId)
          fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/Redeem', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "user_id": this.state.userId,
              "points": this.state.message
            })
          }).then((response) => response.json())
            .then((responseData) => {
              if (responseData.status === "Success") {
                Alert.alert(responseData.message)
                this.setModalTradeVisible(!this.state.modalRedeemVisible);
                this.GetpointsAPI()
              }
              else {
                this.setModalTradeVisible(!this.state.modalRedeemVisible);
              }
              this.setState({
                isFetching: false,
              }, function () {
              });
            })
        })
    }
  }
  _onPressRedeemCancelButton() {
    this.setModalTradeVisible(!this.state.modalRedeemVisible);

  }
  _onPressTradeCancelButton() {
    this.setModalVisible(!this.state.modalTradeVisible);
  }
  _onPressTradeButton() {
    if (this.state.TradePoint == '' || this.state.FaceValue == ''  || this.state.TradeValue == '') {
      Alert.alert("Fild is blank")
    }
    else {
      AsyncStorage.getItem('Id')
        .then((value) => {
          this.state.userId = value
          console.log("userId", this.state.userId)
          fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/Trade', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "user_id": this.state.userId,
              "points_offered": this.state.TradePoint,
              "face_value": this.state.FaceValue,
              "offer_price": this.state.TradeValue
            })
          }).then((response) => response.json())
            .then((responseData) => {
              if (responseData.status === "Success") {
                Alert.alert(responseData.message)
                this.setModalVisible(!this.state.modalTradeVisible);
                this.GetpointsAPI()
              }
              else {
                this.setModalVisible(!this.state.modalTradeVisible);
              }
              this.setState({
                isFetching: false,
              }, function () {
              });
            })
        })
    }
  }
  keyExtractor = (item, index) => index.toString()

  renderItem = ({ item }) => (
    <View style={styles.fixToTextone}>
      <Text style={{ paddingLeft: 15, textShadowColor: '#fff', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15, height: 40, }}>{item.first_name}</Text>
      <Text style={{ textShadowColor: '#fff', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15, height: 40, }}>{item.role_name}</Text>
      <Text style={{ textShadowColor: '#fff', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15, height: 40, }}>{item.last_name}</Text>
      <Text style={{ textShadowColor: '#fff', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15, height: 40, }}>{item.assigned_points}</Text>

    </View>


  )
  onRefresh() {
    this.setState({ isLoading: true }, function() {  this.GetpointsAPI()
      
    });
 }
GetpointsAPI(){
  this.setState({ isLoading: true })
  AsyncStorage.getItem('Id')
  .then((value) => {
    this.state.RoleId = value
    console.log("rpll", this.state.RoleId)

    fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/Points', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "user_id": this.state.RoleId,
      })
    }).then((response) => response.json())
      .then((responseData) => {
        console.log("responseData", responseData)
        if (responseData.status === "Success") {
          this.state.pointArry = responseData.data
          console.log("pointArry:", this.state.pointArry)
          this.setState({ isLoading: false })
          if(responseData[0].total_assigned_points == null){
            this.state.total_assigned_points = "0"
          }
          else{
            this.state.total_assigned_points = responseData[0].total_assigned_points
          }
          this.state.total_vested_points = responseData[0].total_vested_points
          this.state.usable_vested_points = responseData[0].usable_vested_points
          this.state.facevalue = responseData[0].facevalue
          console.log("facevalue:", this.state.facevalue)
          console.log("usable_vested_points:", this.state.total_vested_points)
        }
        else if (responseData.status === "Failed") {
          this.state.pointView = responseData.statu
          console.log("gfdhghfcghf:", responseData.message)
          //Alert.alert(responseData.message);
        }
        this.setState({
          isFetching: false,
        }, function () {
        });
      })
  });
}
componentWillMount(){
  this.GetpointsAPI()
  // setInterval(() => {
  //   this.setState(() => {
  //     this.GetpointsAPI()
  //     return { unseen: "does not display" }
  //   });
  // }, 3000);
}
  
  FlatListHeader = () => {
    return (
      <View elevation={1}
        style={{
          flexDirection: 'row',
          height: 40,
          margin: 1,
          backgroundColor: "#444444",
          borderColor: "black",
          alignSelf: "center",
          shadowOpacity: 1,

        }}
      >
        <Text style={{ paddingLeft: 15, textShadowColor: 'black', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15 }}>Name</Text>
        <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15 }}>Type</Text>
        <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15 }}>Status</Text>
        <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15 }}>Points</Text>
      </View>
    );
  }
  TradeVestedPoint() {
    if (this.state.total_vested_points == 0) {
      return <View style={{ backgroundColor: '#5F5F5F', marginTop: 5 }}>

        <Text style={{
          textAlign: 'center', color: 'white',
          fontSize: 20, fontSize: 18, marginTop: 13,
          height: 40, alignItems: 'center',
          justifyContent: 'center',

        }} >You do not have Sufficient Points to Trade</Text>
      </View>
      

    }
    else {
      return <View  style={{height:60,justifyContent:'space-around',flexDirection:'row',marginLeft:5,marginRight:5}}>
        <TouchableOpacity  onPress={() => {
          this.setModalVisible(true);
        }}>
           <View style={{
           marginTop:5,width:200,
            height: 50,  alignItems: 'center',
            justifyContent: 'center', backgroundColor: '#F1803A', borderRadius: 25,
          }}>
            <Text style={{
              textAlign: 'center', color: 'white',
              fontSize: 17,
              alignItems: 'center',
              justifyContent: 'center'

            }} >Trade vested points</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity  onPress={() => this.props.navigation.navigate('Tradehistory')}>

          <View style={{
           marginTop:5,width:200,
            height: 50,  alignItems: 'center',
            justifyContent: 'center'
          }}>
            <Text style={{
              textAlign: 'center', color: '#3268a8',
              fontSize: 17,
              alignItems: 'center',
              justifyContent: 'center',
              textDecorationLine: 'underline'


            }} >Trade History</Text>
          </View>
        </TouchableOpacity>
      </View>
    }
  }
  RedeemVestedPoint() {
    if (this.state.total_vested_points == 0) {
      return <View style={{backgroundColor: '#5F5F5F',}}>
        <Text style={{
          textAlign: 'center', color: 'white',
          fontSize: 20, fontSize: 18, marginTop: 13,
          height: 40, alignItems: 'center',
          justifyContent: 'center',

        }} >You do not have Sufficient Points to Redeem</Text>
      </View>
    }
    else {
      return <View  style={{height:60,justifyContent:'space-around',flexDirection:'row'}}>
        <TouchableOpacity  onPress={() => {
          this.setModalTradeVisible(true);
        }}>

          <View style={{
           marginTop:5,width:200,
            height: 50,  alignItems: 'center',
            justifyContent: 'center', backgroundColor: '#F1803A', borderRadius: 25,
          }}>
            <Text style={{
              textAlign: 'center', color: 'white',
              fontSize: 17,
              alignItems: 'center',
              justifyContent: 'center'

            }} >Redeem vested points</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity  onPress={() => this.props.navigation.navigate('Redemhistory')}>

          <View style={{
           marginTop:5,width:200,
            height: 50,  alignItems: 'center',
            justifyContent: 'center'
          }}>
            <Text style={{
              textAlign: 'center', color: '#3268a8',
              fontSize: 17,
              alignItems: 'center',
              justifyContent: 'center',
              textDecorationLine: 'underline'

            }} >Redeem History</Text>
          </View>
        </TouchableOpacity>
      </View>
    }
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#000",
        }}
      />
    );
  }
  render() {

    return (
      <View style={{ flex: 1, backgroundColor: "#444444", }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Dashboard')}>
            <View style={{ top: 50, height: 50, width: 50, paddingLeft: 20 }}>
              <Image style={{ top: 20 }} source={require('../Images/back.png')} resizeMode="cover" />
            </View>
          </TouchableWithoutFeedback>
          <View style={{ marginTop: 26, alignSelf: 'center' }}>
            <Image style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
          </View>
          <TouchableWithoutFeedback >
            <View style={{ top: 50, height: 50, width: 50, }}>

              {/* <Image style={{ top: 14 }} source={require('../Images/trans_add.png')} resizeMode="cover" /> */}
            </View>
          </TouchableWithoutFeedback>
        </View>
        <ScrollView style={{ flex: 1, backgroundColor: "#444444", borderTopWidth: 1, borderTopColor: "#E9883A" }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
            <View style={{ flexDirection: 'column' }}>

              <View style={{ alignItems: 'center' }}>
                <Image style={{ height: 70, width: 70, marginTop: 5 }} source={require('../Images/newshapeb.png')} />
                <Text style={{ textShadowColor: 'black',fontWeight:'bold', color: '#000', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: -45 }}>{this.state.total_assigned_points}</Text>
                <Text style={{
                  textAlign: 'center', color: 'white',
                  fontSize: 16, alignItems: 'center', marginTop: 10,
                  justifyContent: 'center',fontWeight: 'bold'

                }} >Total Assigned Points </Text>
              </View>

            </View>
            <View style={{ flexDirection: 'column' }}>

              <View style={{ alignItems: 'center', }}>
                <Image style={{ height: 70, width: 70, marginTop: 5 }} source={require('../Images/newshapea.png')} />
                <Text style={{ textShadowColor: 'black',fontWeight:'bold', color: '#000', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: -45 }}>{this.state.total_vested_points}</Text>
                <Text style={{
                  textAlign: 'center', color: 'white',
                  fontSize: 16, alignItems: 'center',
                  justifyContent: 'center', marginTop: 10,fontWeight: 'bold'

                }} >Available Vested Points </Text>
              </View>

            </View>
          </View>
          <View style={{ backgroundColor: "#444444", }}>

            <View style={{ backgroundColor: "#444444", }}>
              <View elevation={1}
                style={{
                  flexDirection: 'row',
                  height: 40,
                  margin: 1,
                  marginTop: 20,
                  backgroundColor: "#444444",
                  alignSelf: "center",
                  justifyContent: 'space-around',
                  borderBottomWidth: 0.5,
                  borderBottomColor: '#E9883A'
                }}
              >
                <Text style={{ paddingLeft: 15, textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold' }}>Name</Text>
                <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold' }}>Type</Text>
                <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold' }}>Assigned</Text>
                <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold' }}>Vested</Text>
                <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold' }}>Total</Text>

              </View>

              <FlatList style={{ top: 10, height: height - 500 }}
                keyExtractor={this.keyExtractor}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                
                data={this.state.pointArry}
                renderItem={({ item }) => (
                  <View style={styles.fixToTextone}>
                    <Text style={{ paddingLeft: 15, textShadowColor: '#fff', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: 10,fontWeight: 'bold' }}>{item.first_name}</Text>
                    <Text style={{ textShadowColor: '#fff', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: 10,fontWeight: 'bold' }}>{item.role_name}</Text>
                    <Text style={{ textShadowColor: '#fff', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: 10,fontWeight: 'bold' }}>{item.assigned_points}</Text>
                    <Text style={{ textShadowColor: '#fff', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: 10,fontWeight: 'bold' }}>{item.vested_points}</Text>
                    <Text style={{ textShadowColor: '#fff', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: 10,fontWeight: 'bold' }}>{item.total_points}</Text>

                  </View>



                )}
              />
            < ActivityIndicator style={{ position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'}} size="large" color="#E9883A" animating={this.state.isLoading} />
             
             
              <Modal style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                padding: 10,
              }}
                //animationType="slide"
                transparent={true}
                visible={this.state.modalTradeVisible}
                onRequestClose={() => {
                  alert('Modal has been closed.');
                }}>
                <View style={{ marginTop:Platform.OS === 'ios' ? 140 : 20, backgroundColor: '#444444', height: 315, borderColor: '#fff', borderWidth: 1, margin: 20 }}>
                  <TouchableHighlight
                    onPress={() => {
                      this.setModalVisible(!this.state.modalTradeVisible);
                    }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#E9883A', }}>
                      <Text style={{ height: 30, width: 200, color: '#fff', fontWeight: 'bold', borderRadius: 15, marginTop: 14, paddingLeft: 10, fontSize: 18 }}>Trade Vested Points</Text>
                      <Text style={{ height: 30, width: 30, color: '#fff', borderRadius: 15, marginTop: 17, paddingLeft: 10 }}>X</Text>
                    </View>
                  </TouchableHighlight>
                  <Text style={{ color: '#fff', fontWeight: 'bold', borderRadius: 15, marginTop: 17, paddingLeft: 10,paddingRight:10,fontSize:16,fontWeight: 'bold' }}>Total Available Vested Points {this.state.total_vested_points}, you can trade max {this.state.usable_vested_points} </Text>
                  <Input
                    onChangeText={TradePoint => this.setState({ TradePoint })}
                    autoCapitalize='sentences'
                    keyboardType="number-pad"
                    placeholder='Enter Points to Trade'
                    inputStyle={{ height: 50, color: '#fff' }}
                    placeholderTextColor="#fff"

                  />
                  <Input
                    onChangeText={FaceValue => this.setState({ FaceValue })}
                   // value={this.state.TradePoint * this.state.facevalue}
                    autoCapitalize='sentences'
                    placeholder='Enter Face Value'
                    inputStyle={{ height: 50, color: '#fff' }}
                    placeholderTextColor="#fff"

                  />
                  <Input
                    onChangeText={TradeValue => this.setState({ TradeValue })}
                    autoCapitalize='sentences'
                    keyboardType="number-pad"
                    placeholder='Enter Trade Value'
                    inputStyle={{ height: 50, color: '#fff' }}
                    placeholderTextColor="#fff"

                  />
                  <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                      <TouchableOpacity onPress={() => this._onPressTradeCancelButton()} style={{ backgroundColor: '#000', height: 40, width: 140, borderRadius: 5 }} >
                        <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 16 }}>
                          Close
                            </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                      <TouchableOpacity onPress={() => this._onPressTradeButton()} style={{ backgroundColor: '#E9883A', height: 40, width: 140, borderRadius: 5 }} >
                        <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 16 }}>
                          Trade
                            </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>

              <Modal style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                padding: 10,
              }}
                //animationType="slide"
                transparent={true}
                visible={this.state.modalRedeemVisible}
                onRequestClose={() => {
                  alert('Modal has been closed.');
                }}>
                <View style={{ marginTop: Platform.OS === 'ios' ? 140 : 20, backgroundColor: '#444444', height: 220, borderColor: '#fff', borderWidth: 1, margin: 20 }}>
                  <TouchableHighlight
                    onPress={() => {
                      this.setModalTradeVisible(!this.state.modalRedeemVisible);
                    }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#E9883A', }}>

                      <Text style={{ height: 30, width: 230, color: '#fff', fontWeight: 'bold', borderRadius: 15, marginTop: 10, paddingLeft: 10 ,fontSize:20}}>Redeem Vested Points</Text>

                      <Text style={{ height: 30, width: 30, color: '#fff', borderRadius: 15, marginTop: 17, paddingLeft: 10, fontWeight: 'bold' }}>X</Text>

                    </View>
                  </TouchableHighlight>
                  <Text style={{ color: '#fff', borderRadius: 15, marginTop: 17, paddingLeft: 10, fontSize:16 ,fontWeight: 'bold',paddingRight:10 }}>Available vested points:-{this.state.total_vested_points}, you can redeem max:- {this.state.usable_vested_points}</Text>

                  <Input
                    onChangeText={message => this.setState({ message })}
                    autoCapitalize='sentences'
                    keyboardType='number-pad'
                    placeholder='Enter points to redeem'
                    inputStyle={{ height: 50, color: '#fff' }}
                    placeholderTextColor="#fff"

                  />


                  <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                      <TouchableOpacity onPress={() => this._onPressRedeemCancelButton()} style={{ backgroundColor: '#000', height: 40, width: 140, borderRadius: 5 }} >
                        <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 16 }}>
                          Close
                            </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                      <TouchableOpacity onPress={() => this._onPressRedeemButton()} style={{ backgroundColor: '#E9883A', height: 40, width: 140, borderRadius: 5 }} >
                        <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 16 }}>
                          Redeem
                            </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>

            </View>
          </View>
          {this.RedeemVestedPoint()}
              {this.TradeVestedPoint()}
        </ScrollView>
      </View>


    );
  }
}


