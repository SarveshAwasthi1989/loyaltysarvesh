import React, { PureComponent } from 'react';
import {
  Image, SafeAreaView, Modal, AsyncStorage, FlatList, View, Text, TouchableHighlight,ActivityIndicator, Dimensions, TouchableOpacity,TouchableWithoutFeedback
} from 'react-native';
import { ListItem, Header } from 'react-native-elements'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { Dropdown } from 'react-native-material-dropdown';

let styles = {

  fixToTextone: {
    flexDirection: 'row',
    height: 100,
    margin: 1,
    alignSelf: "center",
    borderBottomWidth: 0.5,
    borderBottomColor: '#E9883A',
    isLoading: false
  },
}
let data = [{
  value: 'Referrer',
}, {
  value: 'Buyer',
}, {
  value: 'Seller',
}, {
  value: 'Landlord',
}, {
  value: 'Sales Associate',
},
];
export default class ExistingTransaction extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      RoleId: '',
      transactionArry: [],
      modalVisible: false,
      confirmpass: '',
      property_types:[],
    }
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  _onPressMoreButton() {
    console.log("role_name", this.state.confirmpass)
    this.setModalVisible(!this.state.modalVisible);

    if (this.state.confirmpass === "Referrer") {
      this.props.navigation.navigate('TransactionReferrer', {
      })
    }
    else if (this.state.confirmpass === "Buyer") {
      this.props.navigation.navigate('TransactionBuyer', {
      })
    }
    else if (this.state.confirmpass === "Seller") {
      this.props.navigation.navigate('TransactionSeller', {
      })
    }
    else if (this.state.confirmpass === "Landlord") {
      this.props.navigation.navigate('TransactionLandlord', {
      })
    }
    else if (this.state.confirmpass === "Sales Associate") {
      this.props.navigation.navigate('TransactionSalesAssociate', {
    })
    }


  }

  keyExtractor = (item, index) => index.toString()


  FlatListHeader = () => {
    return (
      <View elevation={1}
        style={{
          flexDirection: 'row',
          height: 40,
          margin: 1,
          backgroundColor: "#444444",
          borderColor: "black",
          alignSelf: "center",
          shadowOpacity: 1,

        }}
      >
        <Text style={{ paddingLeft: 15, textShadowColor: 'black', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15 }}>Name</Text>
        <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15 }}>Type</Text>
        <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15 }}>Status</Text>
        <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15 }}>Points</Text>
        {/* <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, alignSelf: "center", fontSize: 15 }}>Archive</Text> */}
      </View>
    );
  }
 
  componentDidMount(){
    // fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/settings', {
    //   method: 'GET',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json'
    //   },

    // }).then((response) => response.json())
    //   .then((responseData) => {
    //     this.setState({ isLoading: false });
    //     console.log("settings", responseData[0].property_types)
    //     this.state.property_types = responseData[0].property_types
      
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   })
  }
  componentWillMount(){
    this.GetTransactionsListAPI()
    // setInterval(() => {
    //   this.setState(() => {
    //     this.GetTransactionsListAPI()
    //     return { unseen: "does not display" }
    //   });
    // }, 3000);
  }
  GetTransactionsListAPI(){
    this.setState({ isLoading: true });
    AsyncStorage.getItem('Id')
    .then((value) => {
      this.state.RoleId = value
      console.log("rpll", this.state.RoleId)

      fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/TransactionsList', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "user_id": this.state.RoleId,
        })
      }).then((response) => response.json())
        .then((responseData) => {
          if (responseData.status === "Success") {
            this.state.transactionArry = responseData.data
            console.log("transactionArry:", this.state.transactionArry)
            this.setState({ isLoading: false })

          }
          else {
            //this.props.navigation.navigate('WelcomeScreen')
          }
          this.setState({
            isFetching: false,
          }, function () {
          });
        })
    });
  }
 

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.6,
          width: "100%",
          backgroundColor: "#000",
        }}
      />
    );
  }
  
  LocationonMap(item){
    this.props.navigation.navigate('TransDetails', {
      //NearAry: rowData
    })
  }
  render() {

    return (

      <View style={{ flex: 1, backgroundColor: "#444444", }}>
        <View style={{ backgroundColor: "#444444", }}>
        
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' ,borderBottomWidth:1,borderBottomColor:'#F1803A'}}>
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Dashboard')}>
              <View style={{ top: 50, height: 50, width: 50, paddingLeft: 20 }}>
                <Image style={{ top: 20 }} source={require('../Images/back.png')} resizeMode="cover" />
              </View>
            </TouchableWithoutFeedback>
            <View style={{ marginTop: 26, alignSelf: 'center' }}>
              <Image style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
            </View>
            <TouchableWithoutFeedback onPress={() => {
              this.setModalVisible(true);
            }}>
              <View style={{ top: 50,  height: 50, width: 50, }}>

                <Image style={{ top: 14 }} source={require('../Images/trans_add.png')} resizeMode="cover" />
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <Image style={{ height: 25, width: 25, borderRadius: 50, marginTop: 20 }}
            source={require('../Images/transsicon.png')} />
          <Text style={{ textAlign: 'center', marginTop: 20, color: "#E9883A", fontSize: 20, paddingLeft: 10 }}>
            Transactions
                                </Text>
        </View>
          <View style={{ backgroundColor: "#444444", }}>
            <View elevation={1}
              style={{
                flexDirection: 'row',
                height: 40,
                margin: 1,
                alignSelf: "center",
                borderBottomWidth: 0.5,
                borderBottomColor: '#E9883A'


              }}
            >
             
              <Text style={{ paddingLeft: 15, textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold', marginTop: 10 }}>Name</Text>
              <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold', marginTop: 10 }}>Type</Text>
              <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold', marginTop: 10 }}>Status</Text>
              <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold', marginTop: 10 }}>Points</Text>
              {/* <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold', marginTop: 10 }}>Archive</Text> */}
            </View>
            <FlatList style={{ top: 10, height: height - 400 }}
              keyExtractor={this.keyExtractor}
              ItemSeparatorComponent={this.FlatListItemSeparator}

              data={this.state.transactionArry}
              renderItem={({ item }) => (
                <View style={styles.fixToTextone}>
                  {/* <TouchableOpacity onPress={() => this.LocationonMap(item)}>
                    <Text style={{ paddingLeft: 15, textShadowColor: '#fff', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, height: 40, marginTop: 10 }}>{item.first_name}</Text>
                  </TouchableOpacity> */}
                  <Text style={{ paddingLeft: 15, textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold', marginTop: 25,justifyContent:'center' }}>{item.first_name}</Text>
              <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold', marginTop: 25 }}>{item.role_name}</Text>
              <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold', marginTop: 25 }}>{item.status}</Text>
              {/* <Text style={{ textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold', marginTop: 10 }}>{item.points}</Text> */}
              <View style={{ height: 95, width: 95, alignItems: 'center', }}>
          <Image style={{ height: 70, width: 70, marginTop: 2 }} source={require('../Images/newshapea.png')} />
          <Text style={{  textShadowColor: 'black', color: '#fff', flex: 1, textAlign: 'center', fontSize: 15, fontWeight: 'bold', marginTop: -45 }}>{item.points}</Text>
        </View>
                </View>


              )}
            />
                        <ActivityIndicator style={{ position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'}} size="large" color="#E9883A" animating={this.state.isLoading} />

            {/* <Flatlist
    data={array}
    initialNumToRender={array.length}
    renderItem={({ item }) => <ListItem item={item} />}
/>   */}
            <Modal style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 10,
              backgroundColor: 'blue'
            }}
              //animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                alert('Modal has been closed.');
              }}>
              <View style={{ top: 260, backgroundColor: '#444444', height: 200, borderColor: '#fff', borderRadius: 20, borderWidth: 1, margin: 20 }}>
                <TouchableHighlight style={{ height: 60, width: 60, position: 'absolute', right: 8 }}
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <View style={{ height: 30, width: 30, position: 'absolute', right: 8, top: 8, color: '#000', backgroundColor: '#fff', borderRadius: 15 }}>
                    <Text style={{ height: 30, width: 30, color: '#000', borderRadius: 15, top: 6, paddingLeft: 10 }}>X</Text>

                  </View>
                </TouchableHighlight>
                <Dropdown
                  containerStyle={{ width: width - 60, paddingLeft: 10, marginTop: 40 }}
                  label='Select Transaction Type'
                  textColor={'#fff'}
                  labelFontSize={18}
                  selectedItemColor={'#fff'}
                  itemColor={'#fff'}
                  style={{ color: 'white', paddingLeft: 1, fontSize: 18 }} //for changed text color
                  baseColor="rgba(255, 255, 255, 1)" //for initial text color
                  data={data}
                  onChangeText={confirmpass => this.setState({ confirmpass })}
                  value={this.state.confirmpass}
                />

                <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                  <TouchableOpacity onPress={() => this._onPressMoreButton()} style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }} >
                    <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 18 }}>
                      Transaction
                            </Text>
                  </TouchableOpacity>
                </View>

              </View>
            </Modal>
            <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 30 }}>
              <TouchableOpacity onPress={() => {
                this.setModalVisible(true);
              }} style={{ backgroundColor: '#E9883A', height: 50, width: 300, borderRadius: 5 }} >
                <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 20 }}>
                  Add New Transaction
                            </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
       

      </View>


    );
  }
}


