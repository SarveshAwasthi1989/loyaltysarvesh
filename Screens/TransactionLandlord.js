import { TextInput } from 'react-native-gesture-handler';
import React from 'react';
import {
    Image, Dimensions, TouchableOpacity, ScrollView, View, Text, Alert, TouchableWithoutFeedback, KeyboardAvoidingView, AsyncStorage
} from 'react-native';
import RNGooglePlaces from 'react-native-google-places';

import RNPickerSelect from 'react-native-picker-select';
import { Header, Input } from 'react-native-elements'
import { Dropdown } from 'react-native-material-dropdown';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
let property_types = [{
    value: 'Single-Family Homes',
}, {
    value: 'Condominium',
}, {
    value: 'Townhouse',
},
{
    value: 'Multi-Family Home',
}
];
// let minRant = [{
//     value: '1000',
// }, {
//     value: '1501',
// },
// {
//     value: '2001',
// },
// {
//     value: '2501',
// }
// ];
// let mxnRant = [{
//     value: '1500',
// }, {
//     value: '2000',
// },
// {
//     value: '2500',
// },
// {
//     value: '3000',
// }
// ];

let Bathroom = [{
    value: '1',
}, {
    value: '2',
},
{
    value: '3',
},
{
    value: '4',
}
    ,
{
    value: '5',
}
    ,
{
    value: '6',
}
    ,
{
    value: '7',
}
    ,
{
    value: '8',
}
    ,
{
    value: '9',
}
    ,
{
    value: '10',
}
    ,
{
    value: '11',
}
    ,
{
    value: '12',
}
];
let YearBuilt = [{
    value: '1985',
}, {
    value: '1986',
}, {
    value: '1987',
}, {
    value: '1988',
}, {
    value: '1989',
}, {
    value: '1990',
}, {
    value: '1991',
}, {
    value: '1992',
}, {
    value: '1993',
}, {
    value: '1994',
}, {
    value: '1995',
}, {
    value: '1996',
}, {
    value: '1997',
}, {
    value: '1998',
}, {
    value: '1999',
}, {
    value: '2000',
}, {
    value: '2001',
}, {
    value: '2002',
}, {
    value: '2003',
}, {
    value: '2004',
}, {
    value: '2005',
}, {
    value: '2006',
}, {
    value: '2007',
}, {
    value: '2008',
}, {
    value: '2009',
}, {
    value: '2010',
}, {
    value: '2011',
}, {
    value: '2012',
}, {
    value: '2013',
}, {
    value: '2014',
}, {
    value: '2015',
}, {
    value: '2016',
}, {
    value: '2017',
}, {
    value: '2018',
}, {
    value: '2019',
}, {
    value: '2020',
}, {
    value: '2021',
}, {
    value: '2022',
}, {
    value: '2023',
}, {
    value: '2024',
}, {
    value: '2025',
}, {
    value: '2026',
}, {
    value: '2027',
}

];
class TransactionLandlord extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hidePassword: true,
            secureTextEntry: true,
            isLoading: false,
            timeValue: '',
            propertytype: '',
            minprice: '',
            maxprice: '',
            bathroomText: '',
            bedroomsText: '',
            basementType: '',
            basementStatus: '',
            loanApproved: '',
            parkingno: '',
            builtyear: '',
            RoleId: '',
            userId: '',
            additionalcomments: '',
            location: '',
            city: '',
            statetext: '',
            Zipcode: '',
            fullName: '',
            Photo: '',
            basementtotal: "",
            basementfinished: '',
            min_price_range: [],
            max_price_range: [],


        };

    }
    componentWillMount() {
        fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/settings', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then((response) => response.json())
            .then((responseData) => {
                this.setState({ isLoading: false });
                console.log("settings", responseData[0].min_price_range)
                this.state.min_price_range = responseData[0].min_price_range
                this.state.max_price_range = responseData[0].max_price_range
              
            })
            .catch((error) => {
                console.error(error);
            })
        }
    AddLoaction = () => {
        RNGooglePlaces.openAutocompleteModal("google")
            .then((place) => {
                this.state.location = place.addressComponents[0].name
                console.log("nbnbn  place", place.addressComponents);
                this.state.city = place.addressComponents[1].name
                this.state.statetext = place.addressComponents[2].name
            })
            .catch(error => console.log(error.message));
    }
    setTimeValue = (value) => {
        this.state.timeValue = value
        console.log("this.state.timeValue ", this.state.timeValue)
    }
    componentDidMount() {
        AsyncStorage.getItem('Id')
            .then((value) => {
                this.state.userId = value

                fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/ViewProfile', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "user_id": this.state.userId,
                    })
                }).then((response) => response.json())
                    .then((responseData) => {
                        if (responseData.status === "Success") {
                            this.state.transactionArry = responseData.data
                            let fname = this.state.transactionArry[0].first_name

                            var lanme = this.state.transactionArry[0].last_name
                            this.state.fullName = fname.concat(" ", lanme);
                            this.state.Photo = this.state.transactionArry[0].photo

                        }
                        else {
                            //this.props.navigation.navigate('WelcomeScreen')
                        }
                        this.setState({
                            isFetching: false,
                        }, function () {
                        });
                    })

            });
            setInterval(() => {
                this.setState(() => {
                    console.log(this.state.location);
                    return { unseen: "does not display" }
                });
            }, 1000);
    }
    saveDetails() {
        if (this.state.bedroomsText == '' || this.state.bathroomText == ''
            || this.state.propertytype == '' || this.state.YearBuilt == ''
            || this.state.minprice == '' || this.state.maxprice == ''
            || this.state.additionalcomments == '' || this.state.location == ''
            || this.state.city == '' || this.state.statetext == '' || this.state.zipcode == ''
            || this.state.basementtotal == '' || this.state.basementfinished == '') {
            Alert.alert("Please Enter All the Values.");
        }
        else {
            AsyncStorage.getItem('Id')
                .then((value) => {
                    this.state.userId = value
                    console.log("userId", this.state.userId)
                    fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/CreateTransaction', {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            "user_id": this.state.userId,
                            "role_id": "3",
                            "no_bedrooms": this.state.bedroomsText,
                            "no_bathrooms": this.state.bathroomText,
                            "property_type": this.state.propertytype,
                            "year_built": this.state.YearBuilt,
                            "additional_comments": this.state.additionalcomments,
                            "property_address": this.state.location,
                            "city": this.state.city,
                            "state": this.state.statetext,
                            "zipcode": this.state.zipcode,
                            "basement_total": this.state.basementtotal,
                            "basement_finished": this.state.basementfinished,
                            "rent_expected_min": this.state.minprice,
                            "rent_expected_max": this.state.maxprice,

                        })
                    }).then((response) => response.json())
                        .then((responseData) => {
                            this.setState({ isLoading: false });
                            console.log("ffy", responseData)
                            if (responseData.status === "Success") {
                                Alert.alert(JSON.stringify(responseData.message));
                                this.props.navigation.navigate('Dashboard', {})
                            }
                            else {
                                Alert.alert("error");

                            }

                        })
                        .catch((error) => {
                            console.error(error);
                        })
                })

        }
    }

    render() {

        return (
            <View style={{ flex: 1 }}>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 1, borderBottomColor: '#F1803A', backgroundColor: '#444444' }}>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ExistingTransaction')}>
                        <View style={{ marginTop: 50, height: 50, width: 50, paddingLeft: 20 }}>
                            <Image source={require('../Images/profileback.png')} resizeMode="cover" />
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={{ marginTop: 20, alignSelf: 'center' }}>
                        <Image style={{ height: 80, width: 200 }} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
                    </View>
                    <View style={{ marginTop: 50, height: 50, width: 50, }}>
                    </View>
                </View>
                <ScrollView
                    style={{ flex: 1, backgroundColor: '#444444' }}
                    keyboardShouldPersistTaps='handled' >
                    <View style={{ alignItems: 'center' }}>
                        <Image style={{ height: 100, width: 100, borderRadius: 50, marginTop: 20 }}
                            source={{ uri: this.state.Photo }} />
                    </View>
                    <TouchableOpacity>
                        <View style={{ alignItems: 'center' }}>
                            <Image style={{ height: 30, width: 30, borderRadius: 50, marginTop: -25, marginLeft: 80 }}
                                source={require('../Images/profilecam.png')} />
                        </View>
                    </TouchableOpacity>
                    <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 20, color: '#fff' }}>
                        {this.state.fullName}
                    </Text>
                   
                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select Property Types'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={property_types}
                        onChangeText={propertytype => this.setState({ propertytype })}
                        value={this.state.propertytype}
                    />
                    <View>
                    <TouchableOpacity onPress={this.AddLoaction} >
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                marginLeft: 4, marginRight: 5, borderBottomColor: 'grey', borderBottomWidth: 1
                            }}>
                                <View style={{ flexDirection: 'row', height: 60, }}>
                                    <Text style={{ color: '#fff', fontSize: 17, textAlign: 'center', marginTop: 15 }} > {"Enter a location"} </Text>
                                </View>
                                <Text style={{ color: '#fff', marginTop: 15, fontSize: 17 }} > {this.state.location}</Text>
                            </View>
                        </TouchableOpacity>
                        </View>
                    <Input
                        autoCapitalize='none'
                        onChangeText={city => this.setState({ city })}
                        value={this.state.city}
                        inputStyle={{ height: 60, color: '#fff',fontSize: 17 }}
                        placeholderTextColor="#fff"
                        placeholder='City'

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={statetext => this.setState({ statetext })}
                        value={this.state.statetext}
                        inputStyle={{ height: 60, color: '#fff',fontSize: 17 }}
                        placeholderTextColor="#fff"
                        placeholder='State'

                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={Zipcode => this.setState({ Zipcode })}
                        value={this.state.Zipcode}
                        inputStyle={{ height: 60, color: '#fff',fontSize: 17 }}
                        placeholderTextColor="#fff"
                        maxLength={5}
                        keyboardType={'numeric'}
                        placeholder='Zipcode'

                    />

<Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select Min Rent'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={this.state.min_price_range}
                        onChangeText={minprice => this.setState({ minprice })}
                        value={this.state.minprice}
                    />


                 
                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select Max Rent'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={this.state.max_price_range}
                        onChangeText={maxprice => this.setState({ maxprice })}
                        value={this.state.maxprice}
                    />

                 

                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select No. of Bedrooms'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={Bathroom}
                        onChangeText={bedroomsText => this.setState({ bedroomsText })}
                        value={this.state.bedroomsText}
                    />

                    <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select No. of Bathroom'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={Bathroom}
                        onChangeText={bathroomText => this.setState({ bathroomText })}
                        value={this.state.bathroomText}
                    />
                     <Dropdown
                        containerStyle={{ width: width - 20, paddingLeft: 10 }}
                        label='Select Year Built'
                        textColor={'#fff'}
                        labelFontSize={18}
                        selectedItemColor={'#fff'}
                        itemColor={'#fff'}
                        style={{ color: 'white', paddingLeft: 1, fontSize: 17 }} //for changed text color
                        baseColor="rgba(255, 255, 255, 1)" //for initial text color
                        data={YearBuilt}
                        onChangeText={builtyear => this.setState({ builtyear })}
                        value={this.state.builtyear}
                    />
                    <Input
                        autoCapitalize='none'
                        onChangeText={basementtotal => this.setState({ basementtotal })}
                        inputStyle={{ height: 60, color: '#fff',fontSize: 17 }}
                        placeholderTextColor="#fff"
                        keyboardType={'numeric'}
                        placeholder='Basement Total'

                    />

                    <Input
                        autoCapitalize='none'
                        onChangeText={basementfinished => this.setState({ basementfinished })}
                        inputStyle={{ height: 60, color: '#fff',fontSize: 17 }}
                        placeholderTextColor="#fff"
                        keyboardType={'numeric'}
                        placeholder='Basement Finished'

                    />
                   
                   
                  <TextInput style={{ height: 100, margin: 20,fontSize: 18, borderColor: 'gray', borderWidth: 1, borderRadius: 20, color: 'white', paddingLeft: 20 }}
                        onChangeText={additionalcomments => this.setState({ additionalcomments })}
                        value={this.state.additionalcomments}

                    />
                     <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10 }}>
                        <TouchableOpacity onPress={() => this.saveDetails()} style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }}>
                            <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 18 }}>
                                Create Transaction
                            </Text>
                        </TouchableOpacity>
                    </View>
                   
                </ScrollView>
            </View>


        );
    }
}

export default TransactionLandlord;

