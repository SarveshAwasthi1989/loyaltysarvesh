import React, { PureComponent } from 'react';
import {
  Image, StyleSheet, ScrollView, Modal, View, Text, TouchableHighlight, Dimensions, TouchableOpacity, TouchableWithoutFeedback
} from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import FirstRoute from "./ExistingTransaction"
import SecondRoute from "./Archived"
 import { Dropdown } from 'react-native-material-dropdown';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
let data = [{
  value: 'Referrer',
}, {
  value: 'Buyer',
}, {
  value: 'Seller',
}, {
  value: 'Landlord',
}, {
  value: 'Sales Associate',
},
];
export default class Transaction extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'first', title: 'EXISTINGS', color: '#009688' },
        { key: 'second', title: 'ARCHIVED', color: '#3F51B5' },
      ],
      modalVisible: false,
      confirmpass: '',

    };
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  _renderTabBar(props) {
    return (
      <View style={{ marginTop: 2, backgroundColor: "#444444" }}>
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <Image style={{ height: 25, width: 25, borderRadius: 50, marginTop: 20 }}
            source={require('../Images/transsicon.png')} />
          <Text style={{ textAlign: 'center', marginTop: 20, color: "#E9883A", fontSize: 20, paddingLeft: 10 }}>
            Transactions
                                </Text>
        </View>
        <TabBar
          {...props}
          style={{ backgroundColor: '#444444', elevation: 0, height: 60 }}
          indicatorStyle={{ backgroundColor: 'white', height: 3 }}
          renderLabel={({ route }) => (
            <View>
              <Text style={{
                fontSize: 20, textAlign: 'center',
                color: route.key === props.navigationState.routes[props.navigationState.index].key ? 'white' : '#727278'
              }}>
                {route.title}
              </Text>
            </View>
          )}
        />
      </View>
    );
  }


  _onPressMoreButton() {
    console.log("role_name", this.state.confirmpass)
    this.setModalVisible(!this.state.modalVisible);
    if (this.state.confirmpass === "Referrer") {
      this.props.navigation.navigate('TransactionReferrer', {
      })
    }
    else if (this.state.confirmpass === "Buyer") {
      this.props.navigation.navigate('TransactionBuyer', {
      })
    }
    else if (this.state.confirmpass === "Seller") {
      this.props.navigation.navigate('TransactionSeller', {
      })
    }
    else if (this.state.confirmpass === "Landlord") {
      this.props.navigation.navigate('TransactionLandlord', {
      })
    }
    else if (this.state.confirmpass === "Sales Associate") {
      this.props.navigation.navigate('TransactionSalesAssociate', {
      })
    }


  }

  render() {

    return (
      <View style={styles.safeContainer}>
      
        <ScrollView
          style={{
            flex: 1,
            backgroundColor: '#444444'
          }}
          contentContainerStyle={styles.contentContainer}
          keyboardShouldPersistTaps='handled'
        >
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Dashboard')}>
              <View style={{ top: 50, height: 50, width: 50, paddingLeft: 20 }}>
                <Image style={{ top: 20 }} source={require('../Images/back.png')} resizeMode="cover" />
              </View>
            </TouchableWithoutFeedback>
            <View style={{ marginTop: 26, alignSelf: 'center' }}>
              <Image style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
            </View>
            <TouchableWithoutFeedback onPress={() => {
              this.setModalVisible(true);
            }}>
              <View style={{ top: 50,  height: 50, width: 50, }}>

                <Image style={{ top: 14 }} source={require('../Images/trans_add.png')} resizeMode="cover" />
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={styles.container}>

            <TabView
              navigationState={this.state}
              renderScene={SceneMap({
                first: FirstRoute,
                second: SecondRoute,
              })}
              renderTabBar={this._renderTabBar}
              onIndexChange={index => this.setState({ index })}
              initialLayout={{ width: Dimensions.get('window').width }}
            />
            <Modal style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 10,
              backgroundColor: 'blue'
            }}
              //animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                alert('Modal has been closed.');
              }}>
              <View style={{ top: 260, backgroundColor: '#444444', height: 200, borderColor: '#fff', borderRadius: 20, borderWidth: 1, margin: 20 }}>
                <TouchableHighlight style={{ height: 60, width: 60, position: 'absolute', right: 8 }}
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <View style={{ height: 30, width: 30, position: 'absolute', right: 8, top: 8, color: '#000', backgroundColor: '#fff', borderRadius: 15 }}>
                    <Text style={{ height: 30, width: 30, color: '#000', borderRadius: 15, top: 6, paddingLeft: 10 }}>X</Text>

                  </View>
                </TouchableHighlight>
                 <Dropdown
                  containerStyle={{ width: width - 60, paddingLeft: 10, marginTop: 40 }}
                  label='Select Transaction Type'
                  textColor={'#fff'} 
                  labelFontSize={18}
                  selectedItemColor={'#fff'} 
                  itemColor={'#fff'}
                  style={{ color: 'white', paddingLeft: 1, fontSize: 18 }} //for changed text color
                  baseColor="rgba(255, 255, 255, 1)" //for initial text color
                  data={data}
                  onChangeText={confirmpass => this.setState({ confirmpass })}
                  value={this.state.confirmpass}
                /> 

                <View style={{ alignContent: 'center', alignItems: 'center', bottom: 10, marginTop: 20 }}>
                  <TouchableOpacity onPress={() => this._onPressMoreButton()} style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }} >
                    <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 18 }}>
                      Transaction
                            </Text>
                  </TouchableOpacity>
                </View>

              </View>
            </Modal>


          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },
  safeContainer: {
    flex: 1,
    backgroundColor: '#F9F9F9',
  },

  tab: {
    backgroundColor: 'red',
    paddingRight: 5,
    paddingLeft: 20,
    paddingTop: 4,
    marginTop: 2,
  },
  indicator: {
    backgroundColor: 'white',
  },
});


//     keyExtractor = (item, index) => index.toString()

//     renderItem = ({ item }) => (
//       <ListItem
//         title={item.name}
//         subtitle={item.subtitle}
//         leftAvatar={{ source: { uri: item.avatar_url } }}
//         bottomDivider
//         //chevron
//       />
//     )
//     render() {

//         return (

//             <View style={{ flex: 1>
//             <Header
//             statusBarProps={{ barStyle: 'light-content' }}
//             barStyle="light-content" // or directly
//             leftComponent={<TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Dashboard')}>
//                 <View >
//                   <Image  source={require('../Images/down-arrow.png')}  resizeMode="cover" />
//                 </View>
//               </TouchableWithoutFeedback>}
//             centerComponent={
//             <View >
//               <Image  source={require('../Images/logo.png')}  resizeMode="cover" />
//             </View>
//           }
//           rightComponent={<TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Transactionuser')}>
//             <View >
//               <Image source={require('../Images/plus.png')} resizeMode="cover" />
//             </View>
//           </TouchableWithoutFeedback>}
//             containerStyle={{
//               backgroundColor: '#444444',
//               justifyContent: 'space-around',
//             }}
//           />
//                 <View>
//                 <Text style={{ textAlign: 'center', marginTop: 20 }}>
//                             Transactions
//                                 </Text>
//                 <FlatList style={{backgroundColor: '#444444'}}
//       keyExtractor={this.keyExtractor}
//       data={list}
//       renderItem={this.renderItem}
//     />
//                 </View>
//             </View>

//         );
//     }
// }

// export default Transaction;

// const styles = StyleSheet.create({
//     scene: {
//       flex: 1,
//     },
//   });