import React from 'react';
import {
  Image, SafeAreaView, TouchableOpacity, ScrollView, View,Alert, Text, AsyncStorage, TouchableWithoutFeedback,KeyboardAvoidingView
} from 'react-native';
import { Input } from 'react-native-elements'
import ImagePicker from 'react-native-image-picker';
import { TextInputMask } from 'react-native-masked-text'
const keyboardVerticalOffset = Platform.OS === 'ios' ? 10 : 0

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hidePassword: true,
      PickerValueHolder: '',
      avatarSource: null,
      transactionArry: '',
      fastName: '',
      lastname: '',
      email: '',
      Password: '',
      phoneno: '',
      profileimag:'',
      ROOlid:''

    }
    this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
  }
  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };
        this.uploadImage(response.uri)
        this.setState({
          avatarSource: source,
        });
      }
    });
  }
  uploadImage = async (image_uri) => {
    AsyncStorage.getItem('Id')
    .then((value) => {
      this.state.RoleId = value
    let uploadData = new FormData();
    uploadData.append('photo', { type: 'image/jpg', uri: image_uri, name: 'profile.png' })
    uploadData.append("user_id", this.state.RoleId);
    fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/EditPhoto', {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
        'Accept': 'application/json',
      },
      body: uploadData
    }).then((response) => response.json())
      .then((responseData) => {
        console.log("hghjshd",responseData)
        if (responseData.status === "Success") {
          console.log("Profileimage:", responseData.message)
          Alert.alert(responseData.message);
          this.showProfile()
        }
        else {
          Alert.alert("image not upload");
        }
      }).catch(err => {
        console.log(err)
      })
    })

  }
  saveDetails() {
    AsyncStorage.getItem('RoleId')
    .then((value) => {
      this.state.ROOlid = value
      AsyncStorage.getItem('Id')
      .then((value) => {
        this.state.RoleId = value
    console.log("ROOlid", this.state.ROOlid)
    fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/EditProfile', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "user_id": this.state.RoleId,
          "first_name":this.state.fastName,
          "last_name":this.state.lastname,
          "phone":this.state.phoneno,
          "email":this.state.email,
          "password":this.state.Password
        })
      }).then((response) => response.json())
        .then((responseData) => {
          this.setState({ isLoading: false });
          console.log("ffy",responseData)
          if (responseData.status === "Success") {
            Alert.alert(JSON.stringify(responseData.message));
            this.showProfile()
            //this.props.navigation.navigate('Dashboard', {})
          }
          else{

          }
        
        })
        .catch((error) => {
          console.error(error);
        })
      })
    });
  }
showProfile(){
  AsyncStorage.getItem('Id')
  .then((value) => {
    this.state.RoleId = value
  
    fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/ViewProfile', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "user_id": this.state.RoleId,
      })
    }).then((response) => response.json())
      .then((responseData) => {
        if (responseData.status === "Success") {
          this.state.transactionArry = responseData.data
          this.state.fastName = this.state.transactionArry[0].first_name
          this.state.lastname = this.state.transactionArry[0].last_name
          var fname = this.state.transactionArry[0].first_name
            var lanme = this.state.transactionArry[0].last_name
            var fullName = fname.concat(" " , lanme);
          this.state.email = this.state.transactionArry[0].email
          this.state.phoneno = this.state.transactionArry[0].phone
          this.state.profileimag = this.state.transactionArry[0].photo
          AsyncStorage.setItem('Photo', this.state.transactionArry[0].photo);
          AsyncStorage.setItem('fullName', fullName);
          console.log("fname:", this.state.profileimag)

        }
        else {
          //this.props.navigation.navigate('WelcomeScreen')
        }
        this.setState({
          isFetching: false,
        }, function () {
        });
      })
      
  });
}
  componentWillMount() {
    
    this.showProfile()

  }
  setPasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  }
  render() {

    return (

      <View style={{ flex: 1, }}>
         <View style={{ flexDirection: 'row', justifyContent: 'space-between',backgroundColor: "#444444" }}>
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Dashboard')}>
              <View style={{ top: 50, height: 50, width: 50, paddingLeft: 20 }}>
                <Image style={{ top: 20 }} source={require('../Images/back.png')} resizeMode="cover" />
              </View>
            </TouchableWithoutFeedback>
            <View style={{ marginTop: 26, alignSelf: 'center' }}>
              <Image style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
            </View>
            <TouchableWithoutFeedback onPress={() => {
              this.setModalVisible(true);
            }}>
              <View style={{ top: 50,  height: 50, width: 50, }}>

              </View>
            </TouchableWithoutFeedback>
          </View>
        <ScrollView style={{ backgroundColor: "#444444", flex: 1 }}>
        <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={keyboardVerticalOffset}>

          <View style={{ backgroundColor: "#444444", height: 200 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <Image style={{ height: 25, width: 25, borderRadius: 50, marginTop: 20 }}
                source={require('../Images/profileicon.png')} />
              <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 18, color: '#fff', paddingLeft: 10 }}>
                Profile
                                </Text>
            </View>
            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
            <View style={{ alignItems: 'center' }}>

              {this.state.avatarSource === null ? (
                <Image style={{ height: 100, width: 100, borderRadius: 50, marginTop: 20 }}
                source={{ uri: this.state.profileimag }} />
              ) : (
                  <Image style={{ height: 100, width: 100, borderRadius: 50, marginTop: 20 }}
                    source={this.state.avatarSource} />
                )}
            </View>
           
              <View style={{ alignItems: 'center' }}>
                <Image style={{ height: 30, width: 30, borderRadius: 50, marginTop: -25, marginLeft: 80 }}
                  source={require('../Images/profilecam.png')} />
              </View>
            </TouchableOpacity>
            <View>
            {/* <View style={{ alignContent: 'center', alignItems: 'center', marginTop: 40, bottom: 30 }}>
              <TouchableOpacity style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }}>
                <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, fontSize: 18 }}>
                  Change Password
                            </Text>
              </TouchableOpacity>
            </View> */}
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <View>
              <Text style={{ color: "#E9883A", marginLeft: 10, fontSize: 18 }}>
                First Name
                            </Text>
              <Input
                autoCapitalize='none'
                placeholder='First Name'
                inputStyle={{ height: 60, color: '#fff' }}
                placeholderTextColor="#fff"
                onChangeText={fastName => this.setState({ fastName })}
                value={this.state.fastName}

                rightIcon={
                  <Image source={require('../Images/profileedit.png')} />
                }
              />



            </View>
            <View>
              <Text style={{ color: "#E9883A", marginLeft: 10, marginTop: 20, fontSize: 18 }}>
                Last Name
                            </Text>
              <Input
                autoCapitalize='none'
                placeholder='Last Name'
                inputStyle={{ height: 60, color: '#fff' }}
                placeholderTextColor="#fff"
                onChangeText={lastname => this.setState({ lastname })}
                value={this.state.lastname}

                rightIcon={
                  <Image source={require('../Images/profileedit.png')} />
                }
              />
            </View>
            <View>
              <Text style={{ color: "#E9883A", marginLeft: 10, marginTop: 20, fontSize: 18 }}>
                E-mail id
                            </Text>
              <Input
                autoCapitalize='none'
                placeholder='test@gmail.com'
                inputStyle={{ height: 60, color: '#fff' }}
                placeholderTextColor="#fff"
                editable={false}
                onChangeText={email => this.setState({ email })}
                value={this.state.email}


              />
            </View>
            <View>
              <Text style={{ color: "#E9883A", marginLeft: 10, marginTop: 20, fontSize: 18 }}>
                Password
                            </Text>
              <Input
                autoCapitalize='none'
                placeholder='Password'
                inputStyle={{ height: 60, color: '#fff' }}
                placeholderTextColor="#fff"
                onChangeText={Password => this.setState({ Password })}
                value={this.state.Password}
                secureTextEntry={this.state.hidePassword}
                rightIcon={
                  <TouchableOpacity activeOpacity={0.8} onPress={this.setPasswordVisibility}>
                <Image source={(this.state.hidePassword) ? require('../Images/seepass.png') : require('../Images/seepass.png')}  style={{
                 
                }} />
              </TouchableOpacity>
                }
                // rightIcon={
                //   <Image source={require('../Images/profileedit.png')} />
                // }
              />
            </View> 
            {/* <View>
              <Text style={{ color: "#E9883A", marginLeft: 10, marginTop: 20, fontSize: 18 }}>
                Phone Number
                            </Text>
              <Input
                autoCapitalize='none'
                placeholder='Phone Number'
                inputStyle={{ height: 60, color: '#fff' }}
                placeholderTextColor="#fff"
                onChangeText={phoneno => this.setState({ phoneno })}
                value={this.state.phoneno}

                rightIcon={
                  <Image source={require('../Images/profileedit.png')} />
                }
              />
            </View> */}
            <View>
            <Text style={{ color: "#E9883A", marginLeft: 10, marginTop: 20, fontSize: 18 }}>
                Phone Number
                            </Text>
            <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: "grey", paddingRight: 90, height: 50 }}>

                  <TextInputMask
                    type={'custom'}
                    options={{
                     
                      mask: '999-999-9999'
                    }}

                    autoCapitalize='none'
                    placeholder='Phone Number'
                    placeholderTextColor="#fff"
                    value={this.state.phoneno}
                    onChangeText={Phonenumber => {
                      this.setState({
                        Phonenumber
                      })
                    }}

                    style={{
                      height: 50,
                      width: '80%',
                      color: '#fff',
                      paddingLeft: 10,
                      fontSize: 18
                    }}
                  />
                </View>
                </View>
           
            <View style={{ alignContent: 'center', alignItems: 'center', marginTop: 50, bottom: 20 }}>
              <TouchableOpacity onPress={() => this.saveDetails()}style={{ backgroundColor: '#E9883A', height: 40, width: 300, borderRadius: 5 }}>
                <Text style={{ color: '#fff', textAlign: 'center', marginTop: 5, fontSize: 18,height:55 }}>
                  Save Changes
                            </Text>
              </TouchableOpacity>
            </View>
          </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>

    );
  }
}

export default Profile;