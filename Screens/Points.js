import React, { PureComponent } from 'react';
import {
    Image, ScrollView, StyleSheet,TouchableWithoutFeedback, FlatList, View, Text, ActivityIndicator,Dimensions
} from 'react-native';
import { ListItem ,Header} from 'react-native-elements'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import FirstRoute from "./Assigned"
import SecondRoute from "./Vested"

  export default class Points extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'first', title: 'ASSIGNED', color: '#3F51B5' },
        { key: 'second', title: 'VESTED', color: '#009688' },
      ],
    };
  }
 
  _renderTabBar(props) {
    return (
      <View style={{ marginTop: 2,  backgroundColor: "#444444", }}>
           <Text style={{ textAlign: 'center', marginTop: 10,color: "#E9883A",fontSize: 20 , height: 40, }}>
                            Points
                                </Text>
        <TabBar
          {...props}
          style={{ backgroundColor: '#444444', elevation: 0, height: 60 }}
          indicatorStyle={{ backgroundColor: 'white', height: 3 }}
          renderLabel={({ route }) => (
            <View>
              <Text style={{
                fontSize: 20, textAlign: 'center',
                color: route.key === props.navigationState.routes[props.navigationState.index].key ? 'white' : '#727278'
              }}>
                {route.title}
              </Text>
            </View>
          )}
        />
      </View>
    );
  }
  render() {
    return (
      <View style={styles.safeContainer}>
      
          <ScrollView
          style={{
            flex: 1,
            backgroundColor: '#444444'
          }}
          contentContainerStyle={styles.contentContainer}
          keyboardShouldPersistTaps='handled'
        >
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Dashboard')}>
              <View style={{ top: 50, height: 50, width: 50, paddingLeft: 20 }}>
                <Image style={{ top: 20 }} source={require('../Images/back.png')} resizeMode="cover" />
              </View>
            </TouchableWithoutFeedback>
            <View style={{ marginTop: 26, alignSelf: 'center' }}>
              <Image style={{height:80,width:200}} source={require('../Images/logo2.jpeg')} resizeMode="cover" />
            </View>
            <TouchableWithoutFeedback >
              <View style={{ top: 50,  height: 50, width: 50, }}>

                {/* <Image style={{ top: 14 }} source={require('../Images/trans_add.png')} resizeMode="cover" /> */}
              </View>
            </TouchableWithoutFeedback>
          </View>
        <View style={styles.container}>
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              first: FirstRoute,
              second: SecondRoute,
            })}
            renderTabBar={this._renderTabBar}

            onIndexChange={index => this.setState({ index })}
            initialLayout={{ width: Dimensions.get('window').width }}
          />

        </View>
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    top:-1,
   
   
  },
  safeContainer: {
    flex: 1,
    backgroundColor: '#F9F9F9',
  },

  tab: {
    backgroundColor: 'red',
    paddingRight: 5,
    paddingLeft: 20,
    paddingTop: 4,
    marginTop: 2,
  },
  indicator: {
    backgroundColor: 'white',
  },
});


