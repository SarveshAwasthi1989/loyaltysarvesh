import React from 'react';
import {
    Image, Dimensions, TouchableOpacity, ScrollView, View, Text, FlatList,TouchableWithoutFeedback
} from 'react-native';
import { Header,Input} from 'react-native-elements'
import RBSheet from "react-native-raw-bottom-sheet";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const list = [
    {
      name: 'Referrer',
      
    },
    {
        name: 'Buyer',
       
      },{
        name: 'Seller',
       
      },{
        name: 'Landlord',
       
      },{
        name: 'Sales Associate',
       
      },
   
      
  ]
class SelectTransaction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          RolesArry :[],
        }
      }
      componentWillMount(){
        fetch('http://ec2-54-161-177-179.compute-1.amazonaws.com/api/settings', {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          
        }).then((response) => response.json())
          .then((responseData) => {
            this.setState({ isLoading: false });
              this.state.RolesArry = responseData[0].roles
              console.log("roles",this.state.RolesArry)
            
          })
          .catch((error) => {
            console.error(error);
          })
        }
      _onPressMoreButton(rowData){
        console.log("role_name",rowData.name)
        console.log("role_id",rowData.id)
        if(rowData.name === "Referrer"){
          this.props.navigation.navigate('TransactionReferrer', {
          })
        }
        else if(rowData.name === "Buyer"){
          this.props.navigation.navigate('TransactionBuyer', {
          })
        }
        else if(rowData.name === "Seller"){
          this.props.navigation.navigate('TransactionSeller', {
          })
        }
        else if(rowData.name === "Landlord"){
          this.props.navigation.navigate('TransactionLandlord', {
          })
        }
        else if(rowData.name === "Sales Associate"){
          this.props.navigation.navigate('TransactionSalesAssociate', {
          })
        }
        
        
      }
      keyExtractor = (item, index) => index.toString()

      renderItem = ({ item }) => (
        <View >
            <TouchableOpacity 
                          onPress={() => this._onPressMoreButton(item)}>
           <Text style={{ paddingLeft:15, textShadowColor: '#000',color:'#000',   flex: 1, alignSelf: "center",  fontSize: 15,height: 40,}}>{item.name}</Text>
                        </TouchableOpacity>
        </View>
        
         
      )
    render() {
        return (
         <View style={{ flex: 1, }}>
        <Header
            statusBarProps={{ barStyle: 'light-content' }}
            barStyle="light-content" // or directly
            leftComponent={<TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Dashboard')}>
                <View >
                  <Image  source={require('../Images/profileback.png')}  resizeMode="cover" />
                </View>
              </TouchableWithoutFeedback>}
            centerComponent={
            <View >
              <Image  style={{height:80,width:200}} source={require('../Images/logo2.jpeg')}  resizeMode="cover" />
            </View>
          }
         containerStyle={{
              backgroundColor: '#444444',
              justifyContent: 'space-around',
            }}
          />
                    <ScrollView style={{ backgroundColor: "#444444" ,flex:1}}>
                    <TouchableOpacity onPress={() => this.RBSheet.open()}>
              <View style={{ fontSize: 20, fontSize: 16, padding: 15,
                 height: 50,margin:30, alignItems: 'center',top:20,
                  justifyContent: 'center',backgroundColor:'#F1803A',borderRadius: 25 ,}}>
              <Text style={{
                textAlign: 'center',color:'white',
                fontSize: 20, fontSize: 16, padding: 15,
                 height: 50,margin:30, alignItems: 'center',
                  justifyContent: 'center',backgroundColor:'#F1803A',borderRadius: 25 ,
               
              }} >Select Transaction Type</Text>
              </View>
            </TouchableOpacity>

            <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={250}
          duration={250}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center",
              borderRadius: 15 
            }
          }}
        >
          <Text style={{top:-80,textAlign:'left',width:width,height:40,padding:10}}>Choose Language</Text>
          <FlatList 
      keyExtractor={this.keyExtractor}
      // ListHeaderComponent = { this.FlatListHeader } 
      data={list}
      renderItem={this.renderItem}
    />
        </RBSheet>
                    </ScrollView>
            </View>

        );
    }
}

export default SelectTransaction;