/**
 * @format
 */

/**
 * @format
 */
import * as React from 'react';
import { AppRegistry } from 'react-native';
import jj from './App'

import Login from './Screens/Login'
 import Signup from './Screens/Signup'
 import Dashboard from './Screens/Dashboard'
 import NewTransaction from './Screens/NewTransaction'
 import Profile from './Screens/Profile'
 import Addreferee  from './Screens/Addreferee'
 import Transaction from './Screens/Transaction'
 import TransDetails from './Screens/TransDetails'
 import ExistingTransaction from './Screens/ExistingTransaction'
 import TransactionBuyer  from './Screens/TransactionBuyer'
 import TransactionLandlord from './Screens/TransactionLandlord'
 import TransactionReferrer from './Screens/TransactionReferrer'
 import TransactionSalesAssociate from './Screens/TransactionSalesAssociate'
 import TransactionSeller from './Screens/TransactionSeller'
 import Archived from './Screens/Archived'
 import Assigned from './Screens/Assigned'
 import Points from './Screens/Points'
 import ForgotPassword from "./Screens/ForgotPassword"
 import MarketPlace from './Screens/MarketPlace'
 import SelectTransaction from './Screens/SelectTransaction'
 import Redemhistory from './Screens/Redemhistory'
 import Tradehistory from './Screens/Tradehistory'
import { name as appName } from './app.json';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, createSwitchNavigator } from '@react-navigation/native';
import { Icon } from 'react-native-elements'

const Stack = createStackNavigator();
function MyStack(navigation) {
  return (
    <NavigationContainer>{
      /* Rest of your app code */
      <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}
      >
        
        <Stack.Screen name="Login" component={Login} />
         <Stack.Screen name="Signup" component={Signup} />
         <Stack.Screen name="Archived" component={Archived} />
         <Stack.Screen name="Dashboard" component={Dashboard} />
         <Stack.Screen name="Assigned" component={Assigned} />
         
         <Stack.Screen name="NewTransaction" component={NewTransaction}/>
         <Stack.Screen name="Transaction" component={Transaction} />
         <Stack.Screen name="TransDetails" component={TransDetails} />

         
         <Stack.Screen name="SelectTransaction" component={SelectTransaction} />
         <Stack.Screen name="Addreferee" component={Addreferee} />
         <Stack.Screen name="ExistingTransaction" component={ExistingTransaction} />
         <Stack.Screen name="TransactionBuyer" component={TransactionBuyer} />
         <Stack.Screen name="TransactionSeller" component={TransactionSeller} />
         <Stack.Screen name="TransactionLandlord" component={TransactionLandlord} />
         <Stack.Screen name="TransactionReferrer" component={TransactionReferrer} />
         <Stack.Screen name="TransactionSalesAssociate" component={TransactionSalesAssociate} />
         <Stack.Screen name="Profile" component={Profile} />
         <Stack.Screen name="Points" component={Points} />
         <Stack.Screen name="MarketPlace" component={MarketPlace} />
         <Stack.Screen name="Redemhistory" component={Redemhistory} />
         <Stack.Screen name="Tradehistory" component={Tradehistory} />

      <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
   
      </Stack.Navigator>
      
    }
  
    </NavigationContainer>

  );
}




AppRegistry.registerComponent(appName, () => MyStack);


